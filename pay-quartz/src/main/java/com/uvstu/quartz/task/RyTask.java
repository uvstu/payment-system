package com.uvstu.quartz.task;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.pay.client.AliPayClient;
import com.uvstu.common.pay.client.WxPayClient;
import com.uvstu.common.utils.HttpsUtil;
import com.uvstu.system.domain.*;
import com.uvstu.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.uvstu.common.utils.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    @Autowired
    private ISOrderService sOrderService;

    @Autowired
    private ISUorderService isUorderService;

    @Autowired
    private ISApplicationService isApplicationService;

    @Autowired
    private ISStatisticsService isStatisticsService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISSettlementService isSettlementService;


    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }

    /**
     * 系统订单定时同步(10分钟一次)
     */
    public void orderSyn(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try{
                    //执行支付支付订单和退款订单
                    SOrder sOrder = new SOrder();
                    sOrder.setStatus(1);
                    List<SOrder> payOrder = sOrderService.selectOrderData(sOrder);
                    updataPayOrder(payOrder);
                    sOrder.setStatus(4);
                    List<SOrder> refundOrder = sOrderService.selectOrderData(sOrder);
                    updataRefundOrder(refundOrder);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * 用户订单定时同步(10分钟一次)
     */
    public void userOrderSyn(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try{
                    //执行支付支付订单和退款订单
                    SUorder sUorder = new SUorder();
                    sUorder.setStatus(1);
                    List<SUorder> payOrder = isUorderService.selectOrderData(sUorder);
                    updataPayUOrder(payOrder);
                    sUorder.setStatus(4);
                    List<SUorder> refundOrder = isUorderService.selectOrderData(sUorder);
                    updataRefundUOrder(refundOrder);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    /**
     * 订单支付失败处理(15分钟一次)
     */
    public void closeOrder(){
        //支付超时 改成 支付失败状态
        List<SOrder> orderList = sOrderService.selectEndTimeOrder();
        for (int i = 0; i < orderList.size(); i++) {
            orderList.get(i).setStatus(3);
            sOrderService.updateSOrder(orderList.get(i));
        }
        List<SUorder> suOrderList = isUorderService.selectEndTimeOrder();
        for (int i = 0; i < suOrderList.size(); i++) {
            suOrderList.get(i).setStatus(3);
            isUorderService.updateSUorder(suOrderList.get(i));
        }
    }

    /**
     * 今日统计(1小时1次)
     */
    public void todayStatistics(){
        //获取应用列表
        List<SApplication> list = isApplicationService.selectAllSApplication();
        for (int i = 0; i < list.size(); i++) {
            //总金额
            //支付统计
            SStatistics sStatistics = new SStatistics();
            sStatistics.setAppid(list.get(i).getId());
            sStatistics.setUserid(list.get(i).getUserId());
            sStatistics.setType(1l);
            sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            if(sStatistics == null){
                //创建一个空白的
                sStatistics = new SStatistics();
                sStatistics.setAppid(list.get(i).getId());
                sStatistics.setUserid(list.get(i).getUserId());
                sStatistics.setType(1l);
                sStatistics.setMonthAmount(new BigDecimal(0));
                sStatistics.setTotalAmount(new BigDecimal(0));
                JSONArray array = new JSONArray();
                sStatistics.setNearlyXDays(JSONObject.toJSONString(array));
                isStatisticsService.insertSStatistics(sStatistics);
                sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            }

            BigDecimal total_amount = new BigDecimal(0);
            BigDecimal today_amount  = new BigDecimal(0);
            //查询指定应用订单
            SUorder sUorder = new SUorder();
            sUorder.setAppId(list.get(i).getAppId());
            sUorder.setUserId(list.get(i).getUserId());
            sUorder.setStatus(2);
            //获取总金额
            List<SUorder> list1 = isUorderService.selectAllSUorderList(sUorder);
            for (int j = 0; j < list1.size(); j++) {
                total_amount = total_amount.add(list1.get(j).getPrice());
            }
            //本日实时金额
            List<SUorder> list2 = isUorderService.selectTodaySUorderList(sUorder);
            for (int j = 0; j < list2.size(); j++) {
                today_amount = today_amount.add(list2.get(j).getPrice());
            }
            sStatistics.setTotalAmount(total_amount);
            sStatistics.setTodayAmount(today_amount);
            isStatisticsService.updateSStatistics(sStatistics);
            //退款统计
            sStatistics = new SStatistics();
            sStatistics.setAppid(list.get(i).getId());
            sStatistics.setUserid(list.get(i).getUserId());
            sStatistics.setType(2l);
            sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            if(sStatistics == null){
                //创建一个空白的
                sStatistics = new SStatistics();
                sStatistics.setAppid(list.get(i).getId());
                sStatistics.setUserid(list.get(i).getUserId());
                sStatistics.setType(2l);
                sStatistics.setMonthAmount(new BigDecimal(0));
                sStatistics.setTotalAmount(new BigDecimal(0));
                JSONArray array = new JSONArray();
                sStatistics.setNearlyXDays(JSONObject.toJSONString(array));
                isStatisticsService.insertSStatistics(sStatistics);
                sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            }

            total_amount = new BigDecimal(0);
            today_amount  = new BigDecimal(0);
            //查询指定应用订单
            sUorder = new SUorder();
            sUorder.setAppId(list.get(i).getAppId());
            sUorder.setUserId(list.get(i).getUserId());
            sUorder.setStatus(5);
            //获取总金额
            list1 = isUorderService.selectAllSUorderList(sUorder);
            for (int j = 0; j < list1.size(); j++) {
                total_amount = total_amount.add(list1.get(j).getPrice());
            }
            //本日实时金额
            list2 = isUorderService.selectTodaySUorderList(sUorder);
            for (int j = 0; j < list2.size(); j++) {
                today_amount = today_amount.add(list2.get(j).getPrice());
            }
            sStatistics.setTotalAmount(total_amount);
            sStatistics.setTodayAmount(today_amount);
            isStatisticsService.updateSStatistics(sStatistics);
        }
    }

    /**
     * 用户应用资金统计(凌晨1点)
     */
    public void statistics(){
        //获取应用列表
        List<SApplication> list = isApplicationService.selectAllSApplication();
        for (int i = 0; i < list.size(); i++) {
            //支付统计
            SStatistics sStatistics = new SStatistics();
            sStatistics.setAppid(list.get(i).getId());
            sStatistics.setUserid(list.get(i).getUserId());
            sStatistics.setType(1l);
            sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            if(sStatistics == null){
                //创建一个空白的
                sStatistics = new SStatistics();
                sStatistics.setAppid(list.get(i).getId());
                sStatistics.setUserid(list.get(i).getUserId());
                sStatistics.setType(1l);
                isStatisticsService.insertSStatistics(sStatistics);
                sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            }
            BigDecimal total_amount = new BigDecimal(0);
            BigDecimal today_amount  = new BigDecimal(0);
            BigDecimal yesterday_amount  = new BigDecimal(0);
            BigDecimal month_amount  = new BigDecimal(0);
            //查询指定应用订单
            SUorder sUorder = new SUorder();
            sUorder.setAppId(list.get(i).getAppId());
            sUorder.setUserId(list.get(i).getUserId());
            sUorder.setStatus(2);
            //获取总金额
            List<SUorder> list1 = isUorderService.selectAllSUorderList(sUorder);
            for (int j = 0; j < list1.size(); j++) {
                if(list1.get(j).getPrice() != null){
                    total_amount = total_amount.add(list1.get(j).getPrice());
                }
            }
            //获取昨日金额
            List<SUorder> list2 = isUorderService.selectToDaySUorderList(sUorder);
            for (int j = 0; j < list2.size(); j++) {
                if(list2.get(j).getPrice() != null){
                    yesterday_amount = yesterday_amount.add(list2.get(j).getPrice());
                }
            }
            //获取本月金额
            List<SUorder> list3 = isUorderService.selectMonthSUorderList(sUorder);
            for (int j = 0; j < list3.size(); j++) {
                if(list3.get(j).getPrice() != null){
                    month_amount = month_amount.add(list3.get(j).getPrice());
                }
            }
            //近7日  格式：[日期+金额]
            JSONArray array = JSONArray.parseArray(sStatistics.getNearlyXDays());
            if(array != null){
                if(array.size() < 7){
                    JSONObject json = new JSONObject();
                    json.put("time",getYesterDayDate());
                    json.put("amount",yesterday_amount);
                    array.add(json);
                }else{
                    if(JSONObject.toJSONString(array).indexOf(getYesterDayDate()) == -1){
                        for (int j = 0; j < array.size()-1; j++) {
                            array.set(j,array.get(j+1));
                        }
                        JSONObject json = new JSONObject();
                        json.put("time",getYesterDayDate());
                        json.put("amount",yesterday_amount);
                        array.set(array.size()-1,json);
                    }
                }
            }else{
                JSONObject json = new JSONObject();
                json.put("time",getYesterDayDate());
                json.put("amount",yesterday_amount);
                array = new JSONArray();
                array.add(json);
            }
            sStatistics.setTotalAmount(total_amount);
            sStatistics.setTodayAmount(today_amount);
            sStatistics.setYesterdayAmount(yesterday_amount);
            sStatistics.setMonthAmount(month_amount);
            sStatistics.setNearlyXDays(JSONObject.toJSONString(array));
            isStatisticsService.updateSStatistics(sStatistics);

            //退款统计
            sStatistics.setType(2l);
            sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            if(sStatistics == null){
                //创建一个空白的
                sStatistics = new SStatistics();
                sStatistics.setAppid(list.get(i).getId());
                sStatistics.setUserid(list.get(i).getUserId());
                sStatistics.setType(2l);
                isStatisticsService.insertSStatistics(sStatistics);
                sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
            }
            total_amount = new BigDecimal(0);
            today_amount  = new BigDecimal(0);
            yesterday_amount  = new BigDecimal(0);
            month_amount  = new BigDecimal(0);
            //查询指定应用订单
            sUorder.setStatus(5);
            //获取总金额
            list1 = isUorderService.selectAllSUorderList(sUorder);
            for (int j = 0; j < list1.size(); j++) {
                if(list1.get(j).getPrice() != null){
                    total_amount = total_amount.add(list1.get(j).getPrice());
                }
            }
            //获取昨日金额
            list2 = isUorderService.selectToDaySUorderList(sUorder);
            for (int j = 0; j < list2.size(); j++) {
                if(list2.get(j).getPrice() != null){
                    yesterday_amount = yesterday_amount.add(list2.get(j).getPrice());
                }
            }
            //获取本月金额
            list3 = isUorderService.selectMonthSUorderList(sUorder);
            for (int j = 0; j < list3.size(); j++) {
                if(list3.get(j).getPrice() != null){
                    month_amount = month_amount.add(list3.get(j).getPrice());
                }
            }
            array = JSONArray.parseArray(sStatistics.getNearlyXDays());
            //近7日  格式：[日期+金额]
            if(array != null){
                if(array.size() < 7){
                    JSONObject json = new JSONObject();
                    json.put("time",getYesterDayDate());
                    json.put("amount",yesterday_amount);
                    array.add(json);
                }else{
                    if(JSONObject.toJSONString(array).indexOf(getYesterDayDate()) == -1){
                        for (int j = 0; j < array.size()-1; j++) {
                            array.set(j,array.get(j+1));
                        }
                        JSONObject json = new JSONObject();
                        json.put("time",getYesterDayDate());
                        json.put("amount",yesterday_amount);
                        array.set(array.size()-1,json);
                    }
                }
            }else{
                JSONObject json = new JSONObject();
                json.put("time",getYesterDayDate());
                json.put("amount",yesterday_amount);
                array = new JSONArray();
                array.add(json);
            }

            sStatistics.setTotalAmount(total_amount);
            sStatistics.setTodayAmount(today_amount);
            sStatistics.setYesterdayAmount(yesterday_amount);
            sStatistics.setMonthAmount(month_amount);
            sStatistics.setNearlyXDays(JSONObject.toJSONString(array));
            isStatisticsService.updateSStatistics(sStatistics);
        }
    }

    /**
     * 订单到期(每日凌晨1点)
     */
    public void orderExpire(){
        //查询所有订单
        List<SOrder> list = sOrderService.selectAllSuccessOrder();
        for (int i = 0; i < list.size(); i++) {
            //订单全部失效
            list.get(i).setStatus(6);
            sOrderService.updateSOrder(list.get(i));
            //应用状态转换为停用
            SApplication sApplication = new SApplication();
            sApplication.setUserId(list.get(i).getUserId());
            sApplication.setStatus(1);
            isApplicationService.updateSApplicationStatus(sApplication);
        }
    }

    /**
     * 每日结算(每日凌晨1点)
     */
    public void settlement(){
        //获取所有正常用户
        List<SysUser> list = iSysUserService.selectAllUserList();
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getUserId() < 10){
                continue;
            }
            //查询是否生成过结算订单
            SSettlement sSettlement = isSettlementService.selectSSettlementByUserId(list.get(i).getUserId());
            if(sSettlement == null){
                //创建用户结算数据
                sSettlement = new SSettlement();
                sSettlement.setUserId(list.get(i).getUserId());
                sSettlement.setOneDays("0.00");
                sSettlement.setTowDays("0.00");
                sSettlement.setSettlementStatus("1");
                isSettlementService.insertSSettlement(sSettlement);
                sSettlement = isSettlementService.selectSSettlementByUserId(list.get(i).getUserId());
            }
            //查询订单
            SOrder sOrder = new SOrder();
            sOrder.setUserId(list.get(i).getUserId());
            List<SOrder> sOrderList = sOrderService.selectUserOrder(sOrder);
            if(sOrderList.size() == 0){
                //无订单的结算处理
                sSettlement.setSettlementStatus("2");
                sSettlement.setTowDays(sSettlement.getOneDays());
                sSettlement.setOneDays("0.00");
                isSettlementService.updateSSettlement(sSettlement);
            }else{
                //有订单的结算处理
                //查询该用户所有应用
                SApplication sApplication = new SApplication();
                sApplication.setUserId(list.get(i).getUserId());
                List<SApplication> sApplicationList = isApplicationService.selectSApplicationList(sApplication);
                if(sApplicationList.size() == 0){
                    //无应用的结算处理
                    sSettlement.setSettlementStatus("2");
                    sSettlement.setTowDays(sSettlement.getOneDays());
                    sSettlement.setOneDays("0.00");
                    isSettlementService.updateSSettlement(sSettlement);
                }else{
                    BigDecimal money = new BigDecimal(0);
                    BigDecimal rate = new BigDecimal(100).subtract(sOrderList.get(0).getRate());
                    for (int j = 0; j < sApplicationList.size(); j++) {
                        //查询支付统计数据
                        SStatistics sStatistics = new SStatistics();
                        sStatistics.setAppid(sApplicationList.get(j).getId());
                        sStatistics.setType(1L);
                        sStatistics.setUserid(list.get(i).getUserId());
                        sStatistics = isStatisticsService.selectSStatisticsByAppid(sStatistics);
                        if(sStatistics.getYesterdayAmount() != null){
                            money = money.add(sStatistics.getYesterdayAmount());
                        }
                    }
                    rate = rate.divide(new BigDecimal(100));
                    sSettlement.setSettlementStatus("1");
                    sSettlement.setTowDays(sSettlement.getOneDays());
                    if(money.compareTo(BigDecimal.ZERO) == 0){
                        sSettlement.setSettlementStatus("2");
                        sSettlement.setOneDays(new BigDecimal(0).toString());
                    }else{
                        sSettlement.setOneDays(String.valueOf((money.multiply(rate)).setScale(2,BigDecimal.ROUND_DOWN)));
                    }
                    isSettlementService.updateSSettlement(sSettlement);
                }
            }
        }
    }

    /**
     * 获取昨日日期
     * @return
     */
    public String getYesterDayDate(){
        Date d=new Date(System.currentTimeMillis()-1000*60*60*24);
        SimpleDateFormat sp=new SimpleDateFormat("yyyy-MM-dd");
        String time=sp.format(d);
        return time;
    }

    /**
     * 系统订单-支付订单同步
     * @param sOrder
     */
    public void updataPayOrder(List<SOrder> sOrder){
        for (int i = 0; i < sOrder.size(); i++) {
            switch (sOrder.get(i).getPayMethod()){
                case 1:
                    try{
                        Map<String,Object> map = AliPayClient.payQuery(sOrder.get(i).getOrderId(),sOrder.get(i).getTradeNo());
                        if(map.get("alipay_trade_query_response") != null){
                            JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(map.get("alipay_trade_query_response")));
                            if("TRADE_SUCCESS".equals(result.getString("trade_status"))){
                                //支付成功
                                sOrder.get(i).setTradeNo(result.getString("trade_no"));
                                sOrder.get(i).setStatus(2);
                                sOrderService.updateSOrder(sOrder.get(i));
                                //更新用户应用支付方式
                                SApplication sApplication = new SApplication();
                                sApplication.setPayMethod(Integer.valueOf(sOrder.get(i).getPayChannel()));
                                sApplication.setUserId(sOrder.get(i).getUserId());
                                isApplicationService.updateSApplicationPayMethod(sApplication);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try{
                        Map<String,String> result = WxPayClient.payQuery(sOrder.get(i).getTradeNo());
                        if("SUCCESS".equals(result.get("trade_state"))){
                            //支付成功
                            sOrder.get(i).setStatus(2);
                            sOrderService.updateSOrder(sOrder.get(i));
                            //更新用户应用支付方式
                            SApplication sApplication = new SApplication();
                            sApplication.setPayMethod(Integer.valueOf(sOrder.get(i).getPayChannel()));
                            sApplication.setUserId(sOrder.get(i).getUserId());
                            isApplicationService.updateSApplicationPayMethod(sApplication);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * 系统订单-退款订单同步
     * @param sOrder
     */
    public void updataRefundOrder(List<SOrder> sOrder){
        for (int i = 0; i < sOrder.size(); i++) {
            switch (sOrder.get(i).getPayMethod()){
                case 1:
                    try{
                        Map<String,Object> map = AliPayClient.refundQuery(sOrder.get(i).getOrderId(),sOrder.get(i).getTradeNo());
                        if(map.get("alipay_trade_fastpay_refund_query_response") != null){
                            JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(map.get("alipay_trade_fastpay_refund_query_response")));
                            if("REFUND_SUCCESS".equals(result.getString("refund_status"))){
                                //退款成功
                                sOrder.get(i).setStatus(5);
                                sOrderService.updateSOrder(sOrder.get(i));
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try{
                        Map<String,String> result = WxPayClient.refundQuery(sOrder.get(i).getRefundId());
                        if("SUCCESS".equals(result.get("result_code"))){
                            //支付成功
                            sOrder.get(i).setStatus(5);
                            sOrderService.updateSOrder(sOrder.get(i));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * 用户订单-同步支付订单
     * @param payOrder
     */
    private void updataPayUOrder(List<SUorder> payOrder) {
        for (int i = 0; i < payOrder.size(); i++) {
            switch (payOrder.get(i).getPayMethod()){
                case 1:
                    try{
                        Map<String,Object> map = AliPayClient.payQuery(payOrder.get(i).getOutTradeNo(),payOrder.get(i).getTradeNo());
                        if(map.get("alipay_trade_query_response") != null){
                            JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(map.get("alipay_trade_query_response")));
                            if("TRADE_SUCCESS".equals(result.getString("trade_status"))){
                                //支付成功
                                payOrder.get(i).setTradeNo(result.getString("trade_no"));
                                payOrder.get(i).setStatus(2);
                                isUorderService.updateSUorder(payOrder.get(i));
                                //发送通知
                                SUorder sUorder = payOrder.get(i);
                                //查询应用
                                SApplication sApplication = isApplicationService.selectSApplicationByAppid(sUorder.getAppId());
                                //通知第三方用户支付结果
                                JSONObject data = new JSONObject();
                                data.put("appid",sUorder.getAppId());
                                data.put("outTradeNo",sUorder.getOutTradeNo());
                                data.put("tradeNo",sUorder.getTradeNo());
                                data.put("refundNo",sUorder.getRefundNo());
                                data.put("goodName",sUorder.getGoodName());
                                data.put("payMethod",sUorder.getPayMethod());
                                data.put("price",sUorder.getPrice());
                                data.put("transactionTime",sUorder.getTransactionTime());
                                data.put("status",sUorder.getStatus());
                                data.put("refundTime",sUorder.getRefundTime());
                                data.put("createTime",sUorder.getCreateTime());
                                HttpsUtil.doPost(sApplication.getNotifyUrl(),JSONObject.toJSONString(data));
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try{
                        Map<String,String> result = WxPayClient.payQuery(payOrder.get(i).getTradeNo());
                        if("SUCCESS".equals(result.get("trade_state"))){
                            //支付成功
                            payOrder.get(i).setStatus(2);
                            isUorderService.updateSUorder(payOrder.get(i));
                            //发送通知
                            SUorder sUorder = payOrder.get(i);
                            //查询应用
                            SApplication sApplication = isApplicationService.selectSApplicationByAppid(sUorder.getAppId());
                            //通知第三方用户支付结果
                            JSONObject data = new JSONObject();
                            data.put("appid",sUorder.getAppId());
                            data.put("outTradeNo",sUorder.getOutTradeNo());
                            data.put("tradeNo",sUorder.getTradeNo());
                            data.put("refundNo",sUorder.getRefundNo());
                            data.put("goodName",sUorder.getGoodName());
                            data.put("payMethod",sUorder.getPayMethod());
                            data.put("price",sUorder.getPrice());
                            data.put("transactionTime",sUorder.getTransactionTime());
                            data.put("status",sUorder.getStatus());
                            data.put("refundTime",sUorder.getRefundTime());
                            data.put("createTime",sUorder.getCreateTime());
                            HttpsUtil.doPost(sApplication.getNotifyUrl(),JSONObject.toJSONString(data));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * 用户订单-同步退款订单
     * @param refundOrder
     */
    private void updataRefundUOrder(List<SUorder> refundOrder) {
        for (int i = 0; i < refundOrder.size(); i++) {
            switch (refundOrder.get(i).getPayMethod()){
                case 1:
                    try{
                        Map<String,Object> map = AliPayClient.refundQuery(refundOrder.get(i).getOutTradeNo(),refundOrder.get(i).getTradeNo());
                        if(map.get("alipay_trade_fastpay_refund_query_response") != null){
                            JSONObject result = JSONObject.parseObject(JSONObject.toJSONString(map.get("alipay_trade_fastpay_refund_query_response")));
                            if("REFUND_SUCCESS".equals(result.getString("refund_status"))){
                                //退款成功
                                refundOrder.get(i).setStatus(5);
                                isUorderService.updateSUorder(refundOrder.get(i));
                                //发送通知
                                SUorder sUorder = refundOrder.get(i);
                                //查询应用
                                SApplication sApplication = isApplicationService.selectSApplicationByAppid(sUorder.getAppId());
                                //通知第三方用户支付结果
                                JSONObject data = new JSONObject();
                                data.put("appid",sUorder.getAppId());
                                data.put("outTradeNo",sUorder.getOutTradeNo());
                                data.put("tradeNo",sUorder.getTradeNo());
                                data.put("refundNo",sUorder.getRefundNo());
                                data.put("goodName",sUorder.getGoodName());
                                data.put("payMethod",sUorder.getPayMethod());
                                data.put("price",sUorder.getPrice());
                                data.put("transactionTime",sUorder.getTransactionTime());
                                data.put("status",sUorder.getStatus());
                                data.put("refundTime",sUorder.getRefundTime());
                                data.put("createTime",sUorder.getCreateTime());
                                HttpsUtil.doPost(sApplication.getNotifyUrl(),JSONObject.toJSONString(data));
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try{
                        Map<String,String> result = WxPayClient.refundQuery(refundOrder.get(i).getRefundNo());
                        if("SUCCESS".equals(result.get("result_code"))){
                            //支付成功
                            refundOrder.get(i).setStatus(5);
                            isUorderService.updateSUorder(refundOrder.get(i));
                            //发送通知
                            SUorder sUorder = refundOrder.get(i);
                            //查询应用
                            SApplication sApplication = isApplicationService.selectSApplicationByAppid(sUorder.getAppId());
                            //通知第三方用户支付结果
                            JSONObject data = new JSONObject();
                            data.put("appid",sUorder.getAppId());
                            data.put("outTradeNo",sUorder.getOutTradeNo());
                            data.put("tradeNo",sUorder.getTradeNo());
                            data.put("refundNo",sUorder.getRefundNo());
                            data.put("goodName",sUorder.getGoodName());
                            data.put("payMethod",sUorder.getPayMethod());
                            data.put("price",sUorder.getPrice());
                            data.put("transactionTime",sUorder.getTransactionTime());
                            data.put("status",sUorder.getStatus());
                            data.put("refundTime",sUorder.getRefundTime());
                            data.put("createTime",sUorder.getCreateTime());
                            HttpsUtil.doPost(sApplication.getNotifyUrl(),JSONObject.toJSONString(data));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
