# 支付系统

### 介绍

本平台是基于若依框架开发的免签支付平台，主要为个人开发者，个体商户提供免签支付对接的服务，只要通过简单的配置，就可以通过对外接口完成支付宝、微信的支付、查询、退款，通过创建应用将不同项目的支付统计分开，简化了微信支付宝支付。

 **该项目已经停止维护，请转移至新支付系统** 

 **新支付系统：[https://gitee.com/uvstu/new-payment-system](https://gitee.com/uvstu/new-payment-system)** 

### 软件架构

SpringBoot+Mybatis+Redis+MySQL+Vue

### 安装教程

1.  安装 JDK1.8
2.  启动 pay-admin
3.  安装 node 环境
4.  启动 pay-ui 

### 项目需要调整

1、修改数据库地址

在 pay-admin 模块下 resources 文件夹中 application-druid.yml

2、修改邮箱信息

在 pay-framework 模块 SysRegisterService 类 改成自己的 邮箱地址 和 授权码，支持QQ邮箱。

3、后台默认地址

管理员账号：admin 默认密码：admin@123

4、maven仓库

本项目有用到私库，请下载下方文件放入到maven配置中，然后指定该配置

配置文件下载地址：http://pan.uvstu.com/s/Worf9

5、项目启动成功后，修改 系统管理-参数设置 的 系统地址 和 前端页面地址

系统地址规则：http://域名或IP:8090

前端页面地址：http://域名或IP

注意以上配置遵循规则配置，否则会导致无法正常使用。

### 激活说明

平台内置激活功能，一机一码，启动后可以联系作者进行激活，也可以直接将该功能剔除，也可以找作者专门定制一个激活工具。

### 接口说明

对外请求接口： 支付接口、查询接口、退款接口

对外通知接口：支付结果通知、退款结果通知

接口详情请看wiki文档

### 平台展示

![](doc/img/pay01.png)
![](doc/img/pay02.png)
![](doc/img/pay03.png)
![](doc/img/pay04.png)
![](doc/img/pay05.png)
![](doc/img/pay06.png)
![](doc/img/pay07.png)
![](doc/img/pay08.png)
![](doc/img/pay09.png)
![](doc/img/pay10.png)

### QQ交流群

QQ群：391842995

### 个人博客

[个人博客](http://blog.ct35.top:5213/blog/#/home)

### 附属项目

★ 新支付系统：https://gitee.com/uvstu/new-payment-system

### 维护说明

本系统已经停止更新维护，请各位开发者移步至新支付系统。



 **更多项目请关注微信订阅号查询** 

![](doc/img/qrcode.jpg)
