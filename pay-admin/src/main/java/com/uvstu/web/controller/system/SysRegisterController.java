package com.uvstu.web.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.core.domain.model.RegisterBody;
import com.uvstu.common.utils.StringUtils;
import com.uvstu.framework.web.service.SysRegisterService;
import com.uvstu.system.service.ISysConfigService;

/**
 * 注册验证
 * 
 * @author ruoyi
 */
@RestController
public class SysRegisterController extends BaseController
{
    @Autowired
    private SysRegisterService registerService;

    @Autowired
    private ISysConfigService configService;

    /**
     * 用户注册
     * @param user
     * @return
     */
    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterBody user)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error("当前系统没有开启注册功能！");
        }
        String msg = registerService.register(user);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }

    /**
     * 找回密码
     * @param user
     * @return
     */
    @PostMapping("/forgotpwd")
    public AjaxResult forgotpwd(@RequestBody RegisterBody user)
    {

        String msg = registerService.forgotPwd(user);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }
}
