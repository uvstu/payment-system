package com.uvstu.common.pay.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.uvstu.common.pay.exception.TClientException;

/**
 * @version V1.0
 * @Title: 支付宝配置
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-02 23:01
 */
public class AlipayConfig {

    /**
     * 网关地址
     */
    public static String URL = null;

    /**
     * 应用ID
     */
    public static String ALIPAY_APPID = null;

    /**
     * 应用私钥
     */
    public static String APP_PRIVATE_KEY = null;

    /**
     * 支付宝公钥
     */
    public static String ALIPAY_PUBLIC_KEY = null;

    /**
     * 签名类型
     */
    public static String SIGNTYPE = null;

    /**
     * 请求格式
     */
    public static String FORMAT = null;

    /**
     * 请求编码
     */
    public static String CHARSET = null;

    /**
     * 异步通知地址
     */
    public static String notify_url = null;

    /**
     * 同步通知地址
     */
    public static String return_url = null;

    /**
     * 初始化配置
     *
     * @param url             网关地址
     * @param appid           应用ID
     * @param appPrivateKey   应用私钥
     * @param alipayPublicKey 支付宝公钥
     * @param signType        签名类型
     * @param format          请求格式
     * @param charset         请求编码
     * @param notifyUrl       异步通知地址
     * @param returnUrl       同步通知地址
     */
    public AlipayConfig(String url, String appid, String appPrivateKey, String alipayPublicKey,
                        String signType, String format, String charset, String notifyUrl, String returnUrl) throws TClientException {
        URL = url;
        ALIPAY_APPID = appid;
        APP_PRIVATE_KEY = appPrivateKey;
        ALIPAY_PUBLIC_KEY = alipayPublicKey;
        SIGNTYPE = signType;
        FORMAT = format;
        CHARSET = charset;
        notify_url = notifyUrl;
        return_url = returnUrl;
    }

    /**
     * 获取支付实例对象
     *
     * @return
     */
    public static AlipayClient getAlipayClient() {
        AlipayClient alipayClient = (AlipayClient) new DefaultAlipayClient(URL, ALIPAY_APPID, APP_PRIVATE_KEY, FORMAT, CHARSET, ALIPAY_PUBLIC_KEY, SIGNTYPE);
        return alipayClient;
    }

}
