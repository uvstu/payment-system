package com.uvstu.common.pay.exception;

/**
 * @version V1.0
 * @Title: 自定义异常
 * @author: zhengweibin
 * @Description:
 * @create: 2021-12-30 16:04
 */
public class TClientException extends Exception {

    /**
     * 返回码
     */
    private Integer code;

    /**
     * 请求返回信息描述
     */
    private String message;

    /**
     * 请求是否成功
     */
    private boolean isSucceed;

    public TClientException(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucceed() {
        return isSucceed;
    }

    public void setSucceed(boolean succeed) {
        isSucceed = succeed;
    }
}