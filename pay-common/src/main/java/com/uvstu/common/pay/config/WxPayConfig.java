package com.uvstu.common.pay.config;

import com.uvstu.common.pay.exception.TClientException;

/**
 * @version V1.0
 * @Title: 微信配置-接口版本：v2
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-23 11:44
 */
public class WxPayConfig {

    /**
     * 支付接口url
     */
    public static final String PAY_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 订单查询接口url
     */
    public static final String PAY_QUERY_URL = "https://api.mch.weixin.qq.com/pay/orderquery";

    /**
     * 退款接口url
     */
    public static final String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    /**
     * 退款查询接口url
     */
    public static final String REFUND_QUERY_URL = "https://api.mch.weixin.qq.com/pay/refundquery";

    /**
     * 证书地址
     */
    public static String CERT_PATH;

    /**
     * 证书密码
     */
    public static String CERT_PASSWORD;

    /**
     * 应用ID
     */
    public static String APPID;

    /**
     * API秘钥
     */
    public static String APIKEY;

    /**
     * 商户号
     */
    public static String MCHID;

    /**
     * 支付结果通知地址
     */
    public static String PAY_NOTIFY_URL;


    /**
     * 退款结果通知地址
     */
    public static String REFUND_NOTIFY_URL;


    /**
     * 初始化配置
     *
     * @param appid           应用ID
     * @param appkey          API秘钥
     * @param mchId           商户号
     * @param payNotifyUrl    支付结果通知地址
     * @param refundNotifyUrl 退款结果通知地址
     */
    public static void setConfig(String appid, String appkey, String mchId, String payNotifyUrl, String refundNotifyUrl) throws TClientException {
        try{
            APPID = appid;
            APIKEY = appkey;
            MCHID = mchId;
            PAY_NOTIFY_URL = payNotifyUrl;
            REFUND_NOTIFY_URL = refundNotifyUrl;
        }catch (Exception e){
            throw new TClientException("微信支付初始化参数错误！错误信息："+e.getMessage());
        }
    }

    /**
     * 初始化配置
     *
     * @param appid           应用ID
     * @param appkey          API秘钥
     * @param mchId           商户号
     * @param payNotifyUrl    支付结果通知地址
     * @param refundNotifyUrl 退款结果通知地址
     * @param certPath        证书路径
     * @param certPassword    证书密码
     */
    public static void setConfig(String appid, String appkey, String mchId, String payNotifyUrl, String refundNotifyUrl,String certPath,String certPassword) throws TClientException {
        try{
            APPID = appid;
            APIKEY = appkey;
            MCHID = mchId;
            PAY_NOTIFY_URL = payNotifyUrl;
            REFUND_NOTIFY_URL = refundNotifyUrl;
            CERT_PATH = certPath;
            CERT_PASSWORD = certPassword;
        }catch (Exception e){
            throw new TClientException("微信支付初始化参数错误！错误信息："+e.getMessage());
        }
    }

}
