package com.uvstu.common.upay.pay.config;

import cn.uvtool.exception.TClientException;

/**
 * @version V1.0
 * @Title: 微信配置-接口版本：v2
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-23 11:44
 */
public class WxPayConfigUtil {

    /**
     * 支付接口url
     */
    public static final String PAY_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 订单查询接口url
     */
    public static final String PAY_QUERY_URL = "https://api.mch.weixin.qq.com/pay/orderquery";

    /**
     * 退款接口url
     */
    public static final String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    /**
     * 退款查询接口url
     */
    public static final String REFUND_QUERY_URL = "https://api.mch.weixin.qq.com/pay/refundquery";

    /**
     * 证书地址
     */
    public String certPath;

    /**
     * 证书密码
     */
    public String certPassword;

    /**
     * 应用ID
     */
    public String appId;

    /**
     * API秘钥
     */
    public String apiKey;

    /**
     * 商户号
     */
    public String mchId;

    /**
     * 支付结果通知地址
     */
    public String payNotifyUrl;


    /**
     * 退款结果通知地址
     */
    public String refundNotifyUrl;

    public String getCertPath() {
        return certPath;
    }

    public void setCertPath(String certPath) {
        this.certPath = certPath;
    }

    public String getCertPassword() {
        return certPassword;
    }

    public void setCertPassword(String certPassword) {
        this.certPassword = certPassword;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getPayNotifyUrl() {
        return payNotifyUrl;
    }

    public void setPayNotifyUrl(String payNotifyUrl) {
        this.payNotifyUrl = payNotifyUrl;
    }

    public String getRefundNotifyUrl() {
        return refundNotifyUrl;
    }

    public void setRefundNotifyUrl(String refundNotifyUrl) {
        this.refundNotifyUrl = refundNotifyUrl;
    }

    /**
     * 初始化配置
     *
     * @param appid           应用ID
     * @param appkey          API秘钥
     * @param mchId           商户号
     * @param payNotifyUrl    支付结果通知地址
     * @param refundNotifyUrl 退款结果通知地址
     */
    public void setConfig(String appid, String appkey, String mchId, String payNotifyUrl, String refundNotifyUrl) throws TClientException {
        try{
            this.appId = appid;
            this.apiKey = appkey;
            this.mchId = mchId;
            this.payNotifyUrl = payNotifyUrl;
            this.refundNotifyUrl = refundNotifyUrl;
        }catch (Exception e){
            throw new TClientException("微信支付初始化参数错误！错误信息："+e.getMessage());
        }
    }

    /**
     * 初始化配置
     *
     * @param appid           应用ID
     * @param appkey          API秘钥
     * @param mchId           商户号
     * @param payNotifyUrl    支付结果通知地址
     * @param refundNotifyUrl 退款结果通知地址
     * @param certPath        证书路径
     * @param certPassword    证书密码
     */
    public void setConfig(String appid, String appkey, String mchId, String payNotifyUrl, String refundNotifyUrl,String certPath,String certPassword) throws TClientException {
        try{
            this.appId = appid;
            this.apiKey = appkey;
            this.mchId = mchId;
            this.payNotifyUrl = payNotifyUrl;
            this.refundNotifyUrl = refundNotifyUrl;
            this.certPath = certPath;
            this.certPassword = certPassword;
        }catch (Exception e){
            throw new TClientException("微信支付初始化参数错误！错误信息："+e.getMessage());
        }
    }

}
