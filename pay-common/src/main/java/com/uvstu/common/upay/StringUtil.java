package com.uvstu.common.upay;

/**
 * @version V1.0
 * @Title: 字符串校验工具类
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-02 21:52
 */
public class StringUtil {

    /**
     * 字符串不为空
     *
     * @param str
     * @return
     */
    public static boolean isNotNull(String str) {
        if (str == null || str.equals("") || str.equals("null")) {
            return false;
        }
        return true;
    }

    /**
     * 字符串为空
     *
     * @param str
     * @return
     */
    public static boolean isNull(String str) {
        if (str == null || str.equals("") || str.equals("null")) {
            return true;
        }
        return false;
    }

}
