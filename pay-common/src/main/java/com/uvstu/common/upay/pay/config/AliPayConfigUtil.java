package com.uvstu.common.upay.pay.config;

import cn.uvtool.exception.TClientException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;

/**
 * @version V1.0
 * @Title: 支付宝配置
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-02 23:01
 */
public class AliPayConfigUtil {

    /**
     * 网关地址
     */
    public String url = null;

    /**
     * 应用ID
     */
    public String alipayAppid = null;

    /**
     * 应用私钥
     */
    public String appPrivateKey = null;

    /**
     * 支付宝公钥
     */
    public String alipayPublicKey = null;

    /**
     * 签名类型
     */
    public String signType = null;

    /**
     * 请求格式
     */
    public String format = null;

    /**
     * 请求编码
     */
    public String charset = null;

    /**
     * 异步通知地址
     */
    public String notifyUrl = null;

    /**
     * 同步通知地址
     */
    public String returnUrl = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAlipayAppid() {
        return alipayAppid;
    }

    public void setAlipayAppid(String alipayAppid) {
        this.alipayAppid = alipayAppid;
    }

    public String getAppPrivateKey() {
        return appPrivateKey;
    }

    public void setAppPrivateKey(String appPrivateKey) {
        this.appPrivateKey = appPrivateKey;
    }

    public String getAlipayPublicKey() {
        return alipayPublicKey;
    }

    public void setAlipayPublicKey(String alipayPublicKey) {
        this.alipayPublicKey = alipayPublicKey;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    /**
     * 初始化配置
     *
     * @param url             网关地址
     * @param appid           应用ID
     * @param appPrivateKey   应用私钥
     * @param alipayPublicKey 支付宝公钥
     * @param signType        签名类型
     * @param format          请求格式
     * @param charset         请求编码
     * @param notifyUrl       异步通知地址
     * @param returnUrl       同步通知地址
     */
    public AliPayConfigUtil(String url, String appid, String appPrivateKey, String alipayPublicKey,
                            String signType, String format, String charset, String notifyUrl, String returnUrl) throws TClientException {
        this.url = url;
        this.alipayAppid = appid;
        this.appPrivateKey = appPrivateKey;
        this.alipayPublicKey = alipayPublicKey;
        this.signType = signType;
        this.format = format;
        this.charset = charset;
        this.notifyUrl = notifyUrl;
        this.returnUrl = returnUrl;
    }

    /**
     * 获取支付实例对象
     *
     * @return
     */
    public AlipayClient getAlipayClient() {
        AlipayClient alipayClient = (AlipayClient) new DefaultAlipayClient(url, alipayAppid, appPrivateKey, format, charset, alipayPublicKey, signType);
        return alipayClient;
    }

}
