package com.uvstu.common.utils.email;

import cn.uvtool.exception.TClientException;
import com.sun.mail.util.MailSSLSocketFactory;
import com.uvstu.common.pay.utils.StringUtil;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * @version V1.0
 * @Title: 邮件工具类
 * @author: zhengweibin
 * @Description:
 * @create: 2022-12-02 21:52
 */
public class EmailUtil {


    /**
     * 邮件服务地址
     */
    private static String email_host = "";

    /**
     * 邮件发件人
     */
    private static String email_from = "";

    /**
     * 邮件授权密码
     */
    private static String email_password = "";


    /**
     * 发送邮件
     *
     * @param toEmail      收件人邮箱
     * @param emailTitle   邮箱标题
     * @param emailContent 邮箱内容
     * @param isHtml 是否HTML格式  true / false
     * @return
     */
    public static boolean sendEmail(String toEmail, String emailTitle, String emailContent,boolean isHtml) throws TClientException {
        if (StringUtil.isNull(email_from) || StringUtil.isNull(email_host) || StringUtil.isNull(email_password) || StringUtil.isNull(emailTitle) || StringUtil.isNull(emailContent)) {
            throw new TClientException("发送失败，邮件未进行配置！");
        }
        return send(toEmail, emailTitle, emailContent,isHtml);
    }

    /**
     * 邮件配置初始化
     *
     * @param emailHost     邮件服务地址
     * @param emailFrom     发送人
     * @param emailPassword 邮箱授权码
     */
    public static void setConfig(String emailHost, String emailFrom, String emailPassword) {
        email_host = emailHost;
        email_from = emailFrom;
        email_password = emailPassword;
    }

    /**
     * 发送邮件
     *
     * @param toEmail      收件人
     * @param emailTitle   邮件标题
     * @param emailContent 邮件内容
     * @return
     */
    private static boolean send(String toEmail, String emailTitle, String emailContent,boolean isHtml) throws TClientException {
        try {
            //设置收件人
            String to = toEmail;
            //获取系统属性
            Properties properties = System.getProperties();
            //SSL加密
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            properties.put("mail.smtp.ssl.enable", "true");
            properties.put("mail.smtp.ssl.socketFactory", sf);
            properties.setProperty("mail.smtp.port", "465");
            properties.setProperty("mail.smtp.socketFactory.port", "465");
            //设置系统属性
            properties.setProperty("mail.smtp.host", email_host);
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.protocols", "TLSv1.2");

            properties.setProperty("mail.transport.protocol", "SMTP");

            //获取发送邮件会话、获取第三方登录授权码
            Session session = Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(email_from, email_password);
                }
            });
            session.setDebug(true);
            Message message = new MimeMessage(session);
            //防止邮件被当然垃圾邮件处理，披上Outlook的马甲
            message.addHeader("X-Mailer", "Microsoft Outlook Express 6.00.2900.2869");
            message.setFrom(new InternetAddress(email_from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //邮件标题
            message.setSubject(emailTitle);
            BodyPart bodyPart = new MimeBodyPart();
            //邮件内容
            if(isHtml){
                bodyPart.setContent(emailContent, "text/html;charset=utf8");
            }else{
                bodyPart.setText(emailContent);
            }
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            message.setContent(multipart);
            Transport.send(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new TClientException("发送失败，请检查邮件配置！");
        }
    }
}
