package com.uvstu.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class MacUtils {

    /**
     * 银河麒麟
     * @return
     * @throws Exception
     */
    public static String getMAC() throws Exception {
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while(networkInterfaces.hasMoreElements()) {
            NetworkInterface network = networkInterfaces.nextElement();
            byte[] mac = network.getHardwareAddress();
            if(mac == null) {
            } else {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mac.length; i++) {
                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                }
                if (!"".equals(sb.toString())) {
                    return sb.toString();
                }
                break;
            }
        }
        return "";
    }


    /**
     * UOS系统
     * @return
     */
    public static List<String> getMacAddress() {
        ArrayList<String> macs = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            byte[] mac = null;
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                if (netInterface.isLoopback() || netInterface.isVirtual() || netInterface.isPointToPoint() || !netInterface.isUp()) {
                    continue;
                } else {
                    mac = netInterface.getHardwareAddress();
                    if (mac != null) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < mac.length; i++) {
                            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
                        }
                        if (sb.length() > 0) {
                            macs.add(sb.toString().toLowerCase());
                        }
                    }
                }
            }
            return macs;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    /**
     * Linux系统
     * @return
     */
    public static List<String> getLinuxMACAddress() {
        List<String> macs = new ArrayList<>();
        BufferedReader bufferedReader = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec("ifconfig -a");
            bufferedReader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            String line = null;
            int index = -1;
            while ((line = bufferedReader.readLine()) != null) {
                index = line.toLowerCase().indexOf("硬件地址");
                String mac = "";
                if (index != -1) {
                    mac = line.substring(index + 4).trim();
                    macs.add(mac.toLowerCase());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            bufferedReader = null;
            process = null;
        }
        return macs;
    }


    /**
     * windows洗个头
     * @return
     * @throws Exception
     */
    public static List<String> getMACAddressByWindows() throws Exception {
        ArrayList<String> rs = new ArrayList<>();
        String result = "";
        Process process = Runtime.getRuntime().exec("ipconfig /all");
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));
        int index = -1;
        String line;
        while ((line = br.readLine()) != null) {
            index = line.indexOf("物理地址");
            if (index >= 0) {
                index = line.indexOf(":");
                if (index >= 0) {
                    result = line.substring(index + 1).trim();
                }
                rs.add(result.toUpperCase());
            }
        }
        br.close();
        return rs;
    }



}
