import request from '@/utils/request'

// 查询支付配置列表
export function listPay(query) {
  return request({
    url: '/system/pay/list',
    method: 'get',
    params: query
  })
}

// 查询支付配置详细
export function getPay(id) {
  return request({
    url: '/system/pay/' + id,
    method: 'get'
  })
}

// 新增支付配置
export function addPay(data) {
  return request({
    url: '/system/pay',
    method: 'post',
    data: data
  })
}

// 修改支付配置
export function updatePay(data) {
  return request({
    url: '/system/pay',
    method: 'put',
    data: data
  })
}

// 删除支付配置
export function delPay(id) {
  return request({
    url: '/system/pay/' + id,
    method: 'delete'
  })
}
