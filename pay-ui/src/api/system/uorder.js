import request from '@/utils/request'

// 查询用户订单列表
export function listUorder(query) {
  return request({
    url: '/system/uorder/list',
    method: 'get',
    params: query
  })
}

// 查询用户订单详细
export function getUorder(id) {
  return request({
    url: '/system/uorder/' + id,
    method: 'get'
  })
}

// 新增用户订单
export function addUorder(data) {
  return request({
    url: '/system/uorder',
    method: 'post',
    data: data
  })
}

// 修改用户订单
export function updateUorder(data) {
  return request({
    url: '/system/uorder',
    method: 'put',
    data: data
  })
}

// 删除用户订单
export function delUorder(id) {
  return request({
    url: '/system/uorder/' + id,
    method: 'delete'
  })
}

// 退款
export function refund(query) {
  return request({
    url: '/system/uorder/refund',
    method: 'post',
    params: query
  })
}

// 重复通知
export function orderNotice(query) {
  return request({
    url: '/system/uorder/orderNotice',
    method: 'post',
    params: query
  })
}
