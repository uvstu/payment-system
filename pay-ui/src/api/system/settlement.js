import request from '@/utils/request'

// 查询结算管理列表
export function listSettlement(query) {
  return request({
    url: '/system/settlement/list',
    method: 'get',
    params: query
  })
}

// 查询结算管理详细
export function getSettlement(id) {
  return request({
    url: '/system/settlement/' + id,
    method: 'get'
  })
}

// 新增结算管理
export function addSettlement(data) {
  return request({
    url: '/system/settlement',
    method: 'post',
    data: data
  })
}

// 修改结算管理
export function updateSettlement(data) {
  return request({
    url: '/system/settlement',
    method: 'put',
    data: data
  })
}

// 删除结算管理
export function delSettlement(id) {
  return request({
    url: '/system/settlement/' + id,
    method: 'delete'
  })
}

export function getUserSettlement(){
  return request({
    url: '/system/settlement/getUserSettlement',
    method: 'post'
  })
}

export function updataUserSettlement(data){
  return request({
    url: '/system/settlement/updataUserSettlement',
    method: 'post',
    data: data
  })
}

export function getUserBasicData(){
  return request({
    url: '/system/settlement/getUserBasicData',
    method: 'post'
  })
}

