import request from '@/utils/request'

// 查询商品列表
export function listGood(query) {
  return request({
    url: '/system/good/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getGood(id) {
  return request({
    url: '/system/good/' + id,
    method: 'get'
  })
}

// 新增商品
export function addGood(data) {
  return request({
    url: '/system/good',
    method: 'post',
    data: data
  })
}

// 修改商品
export function updateGood(data) {
  return request({
    url: '/system/good',
    method: 'put',
    data: data
  })
}

// 删除商品
export function delGood(id) {
  return request({
    url: '/system/good/' + id,
    method: 'delete'
  })
}

// 查询商品列表
export function getGoodList(query) {
  return request({
    url: '/system/good/getGoodList',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getGoodInfo(id) {
  return request({
    url: '/system/good/getGoodInfo/' + id,
    method: 'get'
  })
}

//获取可升级商品
export function getUpgradeGood(query) {
  return request({
    url: '/system/good/getUpgradeGood',
    method: 'post',
    params: query
  })
}
