import request from '@/utils/request'

// 查询订单列表
export function listOrder(query) {
  return request({
    url: '/system/order/list',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getOrder(id) {
  return request({
    url: '/system/order/' + id,
    method: 'get'
  })
}

// 新增订单
export function addOrder(data) {
  return request({
    url: '/system/order',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateOrder(data) {
  return request({
    url: '/system/order',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delOrder(id) {
  return request({
    url: '/system/order/' + id,
    method: 'delete'
  })
}

//用户订单列表
export function getUserOrderList(query) {
  return request({
    url: '/system/order/getUserOrderList',
    method: 'get',
    params: query
  })
}

//获取用户最新可用订单
export function getUserOrder() {
  return request({
    url: '/system/order/getUserOrder',
    method: 'get'
  })
}

//关闭订单
export function closeOrder(id) {
  return request({
    url: '/system/order/closeOrder?id='+id,
    method: 'get'
  })
}

//订单解绑
export function relieveOrder(id) {
  return request({
    url: '/system/order/relieveOrder?id='+id,
    method: 'get'
  })
}

// 支付宝订单下单
export function zfbPay(id,payNum,type) {
  return request({
    url: '/system/order/aliPay?id='+id+'&effectiveTime='+payNum+"&type="+type,
    method: 'post'
  })
}

//支付宝退款
export function zfbRefund(id) {
  return request({
    url: '/system/order/zfbRefund?id='+id,
    method: 'post'
  })
}


// 微信订单下单
export function wxPay(id,payNum,type) {
  return request({
    url: '/system/order/wxPay?id='+id+'&effectiveTime='+payNum+"&type="+type,
    method: 'post',
  })
}

//支付宝退款
export function wxRefund(id) {
  return request({
    url: '/system/order/wxRefund?id='+id,
    method: 'post'
  })
}

//订单同步中
export function orderSyn() {
  return request({
    url: '/system/order/orderSyn',
    method: 'post'
  })
}
