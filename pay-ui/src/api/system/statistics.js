import request from '@/utils/request'

// 查询用户订单统计列表
export function listStatistics(query) {
  return request({
    url: '/system/statistics/list',
    method: 'get',
    params: query
  })
}

// 查询用户订单统计详细
export function getStatistics(id) {
  return request({
    url: '/system/statistics/' + id,
    method: 'get'
  })
}

// 新增用户订单统计
export function addStatistics(data) {
  return request({
    url: '/system/statistics',
    method: 'post',
    data: data
  })
}

// 修改用户订单统计
export function updateStatistics(data) {
  return request({
    url: '/system/statistics',
    method: 'put',
    data: data
  })
}

// 删除用户订单统计
export function delStatistics(id) {
  return request({
    url: '/system/statistics/' + id,
    method: 'delete'
  })
}

export function getAppIdStatistics(query) {
  return request({
    url: '/system/statistics/getAppIdStatistics',
    method: 'get',
    params: query
  })
}
