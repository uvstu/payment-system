import request from '@/utils/request'

// 获取验证码
export function gkey() {
  return request({
    url: '/system/activation/gkey',
    method: 'post'
  })
}

// 激活平台
export function activation(query) {
  return request({
    url: '/system/activation/activation',
    method: 'post',
    params: query
  })
}

// 平台是否激活
export function checkKeyData() {
  return request({
    url: '/system/activation/checkKeyData',
    method: 'post'
  })
}
