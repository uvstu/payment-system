package com.uvstu.system.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface SysActivationMapper {

    /**
     * 获取激活信息
     */
    Map<String,String> selectActivationData();

    /**
     * 新增密钥
     * @param gkey
     */
    void insertActivation(@Param("gkey") String gkey,@Param("akey")  String akey);

    /**
     * 更新密钥
     * @param gkey
     */
    void updateActivationGkey(String gkey);

    /**
     * 删除所有
     */
    void delAll();

}
