package com.uvstu.system.mapper;

import java.util.List;
import com.uvstu.system.domain.SSettlement;
import com.uvstu.system.domain.SStatistics;

/**
 * 结算管理Mapper接口
 * 
 * @author zwb
 * @date 2023-02-12
 */
public interface SSettlementMapper 
{
    /**
     * 查询结算管理
     * 
     * @param id 结算管理主键
     * @return 结算管理
     */
    public SSettlement selectSSettlementById(Long id);

    /**
     * 查询结算管理列表
     * 
     * @param sSettlement 结算管理
     * @return 结算管理集合
     */
    public List<SSettlement> selectSSettlementList(SSettlement sSettlement);

    /**
     * 新增结算管理
     * 
     * @param sSettlement 结算管理
     * @return 结果
     */
    public int insertSSettlement(SSettlement sSettlement);

    /**
     * 修改结算管理
     * 
     * @param sSettlement 结算管理
     * @return 结果
     */
    public int updateSSettlement(SSettlement sSettlement);

    /**
     * 删除结算管理
     * 
     * @param id 结算管理主键
     * @return 结果
     */
    public int deleteSSettlementById(Long id);

    /**
     * 批量删除结算管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSSettlementByIds(Long[] ids);

    /**
     * 查询用户结算信息
     * @param userId
     * @return
     */
    SSettlement selectSSettlementByUserId(Long userId);
}
