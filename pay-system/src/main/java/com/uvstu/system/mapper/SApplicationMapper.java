package com.uvstu.system.mapper;

import java.util.List;
import com.uvstu.system.domain.SApplication;
import org.apache.ibatis.annotations.Param;

/**
 * 应用管理Mapper接口
 * 
 * @author 郑伟滨
 * @date 2023-02-05
 */
public interface SApplicationMapper 
{
    /**
     * 查询应用管理
     * 
     * @param id 应用管理主键
     * @return 应用管理
     */
    public SApplication selectSApplicationById(Long id);

    /**
     * 查询应用管理列表
     * 
     * @param sApplication 应用管理
     * @return 应用管理集合
     */
    public List<SApplication> selectSApplicationList(SApplication sApplication);

    /**
     * 新增应用管理
     * 
     * @param sApplication 应用管理
     * @return 结果
     */
    public int insertSApplication(SApplication sApplication);

    /**
     * 修改应用管理
     * 
     * @param sApplication 应用管理
     * @return 结果
     */
    public int updateSApplication(SApplication sApplication);

    /**
     * 删除应用管理
     * 
     * @param id 应用管理主键
     * @return 结果
     */
    public int deleteSApplicationById(Long id);

    /**
     * 批量删除应用管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSApplicationByIds(Long[] ids);

    /**
     * 更新用户应用状态
     * @param sApplication
     */
    public void updateSApplicationStatus(SApplication sApplication);

    /**
     * 查询应用
     * @param sApplication
     * @return
     */
    SApplication selectSApplicationByAppid(SApplication sApplication);

    /**
     * 更新指定用户所有应用支付通道
     * @param sApplication
     */
    void updateSApplicationPayMethod(SApplication sApplication);

    /**
     * 查询所有的应用
     * @return
     */
    List<SApplication> selectAllSApplication();
}
