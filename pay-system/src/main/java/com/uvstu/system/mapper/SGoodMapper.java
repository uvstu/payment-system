package com.uvstu.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.uvstu.system.domain.SGood;

/**
 * 商品Mapper接口
 * 
 * @author zwb
 * @date 2023-02-01
 */
public interface SGoodMapper 
{
    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    public SGood selectSGoodById(Long id);

    /**
     * 查询商品列表
     * 
     * @param sGood 商品
     * @return 商品集合
     */
    public List<SGood> selectSGoodList(SGood sGood);

    /**
     * 新增商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    public int insertSGood(SGood sGood);

    /**
     * 修改商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    public int updateSGood(SGood sGood);

    /**
     * 删除商品
     * 
     * @param id 商品主键
     * @return 结果
     */
    public int deleteSGoodById(Long id);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSGoodByIds(Long[] ids);

    /**
     * 获取可升级商品
     * @param price
     * @return
     */
    List<SGood> getUpgradeGood(BigDecimal price);
}
