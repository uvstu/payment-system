package com.uvstu.system.controller;

import com.alibaba.fastjson2.JSONObject;
import com.uvstu.system.service.ISysActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Map;

@RestController
@RequestMapping("/system/activation")
public class SysActivationController {

    @Autowired
    private ISysActivationService iSysActivationService;

    /**
     * 项目启动时，初始化验证码信息，绑定机器
     */
    @PostConstruct
    public void init() {
        iSysActivationService.init();
    }

    /**
     * 获取验证码
     * @return
     */
    @RequestMapping("/gkey")
    public String getGkey(){
        return iSysActivationService.getGkey();
    }

    /**
     * 激活
     * @param gkeyData 验证码
     * @param akeyData 激活码
     * @return
     */
    @RequestMapping("/activation")
    public boolean activation(String gkeyData,String akeyData){
        return iSysActivationService.activation(gkeyData,akeyData);
    }

    /**
     * 校验平台是否激活
     * @return
     */
    @RequestMapping("/checkKeyData")
    public Map<String,Object> checkKeyData(){
        return iSysActivationService.checkKeyData();
    }
}
