package com.uvstu.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.uvstu.common.annotation.Log;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.enums.BusinessType;
import com.uvstu.system.domain.SSettlement;
import com.uvstu.system.service.ISSettlementService;
import com.uvstu.common.utils.poi.ExcelUtil;
import com.uvstu.common.core.page.TableDataInfo;

/**
 * 结算管理Controller
 * 
 * @author zwb
 * @date 2023-02-12
 */
@RestController
@RequestMapping("/system/settlement")
public class SSettlementController extends BaseController
{
    @Autowired
    private ISSettlementService sSettlementService;

    /**
     * 查询结算管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:list')")
    @GetMapping("/list")
    public TableDataInfo list(SSettlement sSettlement)
    {
        startPage();
        List<SSettlement> list = sSettlementService.selectSSettlementList(sSettlement);
        return getDataTable(list);
    }

    /**
     * 导出结算管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:export')")
    @Log(title = "结算管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SSettlement sSettlement)
    {
        List<SSettlement> list = sSettlementService.selectSSettlementList(sSettlement);
        ExcelUtil<SSettlement> util = new ExcelUtil<SSettlement>(SSettlement.class);
        util.exportExcel(response, list, "结算管理数据");
    }

    /**
     * 获取结算管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sSettlementService.selectSSettlementById(id));
    }

    /**
     * 新增结算管理
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:add')")
    @Log(title = "结算管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SSettlement sSettlement)
    {
        return toAjax(sSettlementService.insertSSettlement(sSettlement));
    }

    /**
     * 修改结算管理
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:edit')")
    @Log(title = "结算管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SSettlement sSettlement)
    {
        return toAjax(sSettlementService.updateSSettlement(sSettlement));
    }

    /**
     * 删除结算管理
     */
    @PreAuthorize("@ss.hasPermi('system:settlement:remove')")
    @Log(title = "结算管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sSettlementService.deleteSSettlementByIds(ids));
    }

    /**
     * 获取用户结算数据
     * @return
     */
    @RequestMapping("/getUserSettlement")
    public SSettlement getUserSettlement(){
        return sSettlementService.getUserSettlement();
    }

    /**
     * 获取用户结算数据
     * @return
     */
    @RequestMapping("/updataUserSettlement")
    public AjaxResult updataUserSettlement(@RequestBody SSettlement sSettlement){
        return toAjax(sSettlementService.updataUserSettlement(sSettlement));
    }

    /**
     * 获取用户基础信息
     * @return
     */
    @RequestMapping("/getUserBasicData")
    public AjaxResult getUserBasicData(){
        return success(sSettlementService.getUserBasicData());
    }

}
