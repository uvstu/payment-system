package com.uvstu.system.controller;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import com.uvstu.common.annotation.Log;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.core.page.TableDataInfo;
import com.uvstu.common.enums.BusinessType;
import com.uvstu.common.utils.poi.ExcelUtil;
import com.uvstu.system.domain.SPayConfig;
import com.uvstu.system.service.ISPayConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 支付配置Controller
 * 
 * @author zwb
 * @date 2022-10-05
 */
@RestController
@RequestMapping("/system/pay")
public class SPayConfigController extends BaseController
{
    @Autowired
    private ISPayConfigService sPayConfigService;

    /**
     * 查询支付配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:pay:list')")
    @GetMapping("/list")
    public TableDataInfo list(SPayConfig sPayConfig)
    {
        startPage();
        List<SPayConfig> list = sPayConfigService.selectSPayConfigList(sPayConfig);
        return getDataTable(list);
    }

    /**
     * 导出支付配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:pay:export')")
    @Log(title = "支付配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SPayConfig sPayConfig)
    {
        List<SPayConfig> list = sPayConfigService.selectSPayConfigList(sPayConfig);
        ExcelUtil<SPayConfig> util = new ExcelUtil<SPayConfig>(SPayConfig.class);
        util.exportExcel(response, list, "支付配置数据");
    }

    /**
     * 获取支付配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:pay:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sPayConfigService.selectSPayConfigById(id));
    }

    /**
     * 新增支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:pay:add')")
    @Log(title = "支付配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SPayConfig sPayConfig)
    {
        return toAjax(sPayConfigService.insertSPayConfig(sPayConfig));
    }

    /**
     * 修改支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:pay:edit')")
    @Log(title = "支付配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SPayConfig sPayConfig)
    {
        return toAjax(sPayConfigService.updateSPayConfig(sPayConfig));
    }

    /**
     * 删除支付配置
     */
    @PreAuthorize("@ss.hasPermi('system:pay:remove')")
    @Log(title = "支付配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sPayConfigService.deleteSPayConfigByIds(ids));
    }

    /**
     * 项目启动时，初始化支付配置
     */
    @PostConstruct
    public void init() {
        sPayConfigService.init();
    }

}
