package com.uvstu.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.uvstu.common.annotation.Log;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.enums.BusinessType;
import com.uvstu.system.domain.SStatistics;
import com.uvstu.system.service.ISStatisticsService;
import com.uvstu.common.utils.poi.ExcelUtil;
import com.uvstu.common.core.page.TableDataInfo;

/**
 * 用户订单统计Controller
 * 
 * @author zwb
 * @date 2023-02-09
 */
@RestController
@RequestMapping("/system/statistics")
public class SStatisticsController extends BaseController
{
    @Autowired
    private ISStatisticsService sStatisticsService;

    /**
     * 查询用户订单统计列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SStatistics sStatistics)
    {
        startPage();
        List<SStatistics> list = sStatisticsService.selectSStatisticsList(sStatistics);
        return getDataTable(list);
    }

    /**
     * 导出用户订单统计列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, SStatistics sStatistics)
    {
        List<SStatistics> list = sStatisticsService.selectSStatisticsList(sStatistics);
        ExcelUtil<SStatistics> util = new ExcelUtil<SStatistics>(SStatistics.class);
        util.exportExcel(response, list, "用户订单统计数据");
    }

    /**
     * 获取用户订单统计详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sStatisticsService.selectSStatisticsById(id));
    }

    /**
     * 获取统计详情
     * @param sStatistics
     * @return
     */
    @GetMapping(value = "/getAppIdStatistics")
    public AjaxResult getAppIdStatistics(SStatistics sStatistics)
    {
        return success(sStatisticsService.getAppIdStatistics(sStatistics));
    }


    /**
     * 新增用户订单统计
     */
    @PostMapping
    public AjaxResult add(@RequestBody SStatistics sStatistics)
    {
        return toAjax(sStatisticsService.insertSStatistics(sStatistics));
    }

    /**
     * 修改用户订单统计
     */
    @PutMapping
    public AjaxResult edit(@RequestBody SStatistics sStatistics)
    {
        return toAjax(sStatisticsService.updateSStatistics(sStatistics));
    }

    /**
     * 删除用户订单统计
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sStatisticsService.deleteSStatisticsByIds(ids));
    }
}
