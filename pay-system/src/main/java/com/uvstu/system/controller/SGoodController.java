package com.uvstu.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.uvstu.common.annotation.Log;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.enums.BusinessType;
import com.uvstu.system.domain.SGood;
import com.uvstu.system.service.ISGoodService;
import com.uvstu.common.utils.poi.ExcelUtil;
import com.uvstu.common.core.page.TableDataInfo;

/**
 * 商品Controller
 * 
 * @author zwb
 * @date 2023-02-01
 */
@RestController
@RequestMapping("/system/good")
public class SGoodController extends BaseController
{
    @Autowired
    private ISGoodService sGoodService;

    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('system:good:list')")
    @GetMapping("/list")
    public TableDataInfo list(SGood sGood)
    {
        startPage();
        List<SGood> list = sGoodService.selectSGoodList(sGood);
        return getDataTable(list);
    }

    /**
     * 导出商品列表
     */
    @PreAuthorize("@ss.hasPermi('system:good:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SGood sGood)
    {
        List<SGood> list = sGoodService.selectSGoodList(sGood);
        ExcelUtil<SGood> util = new ExcelUtil<SGood>(SGood.class);
        util.exportExcel(response, list, "商品数据");
    }

    /**
     * 获取商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:good:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sGoodService.selectSGoodById(id));
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('system:good:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SGood sGood)
    {
        return toAjax(sGoodService.insertSGood(sGood));
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('system:good:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SGood sGood)
    {
        return toAjax(sGoodService.updateSGood(sGood));
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('system:good:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sGoodService.deleteSGoodByIds(ids));
    }

    /**
     * 查询商品列表
     * @param sGood
     * @return
     */
    @GetMapping("/getGoodList")
    public TableDataInfo getGoodList(SGood sGood)
    {
        startPage();
        List<SGood> list = sGoodService.selectSGoodList(sGood);
        return getDataTable(list);
    }

    /**
     * 获取商品详细信息
     */
    @GetMapping(value = "/getGoodInfo/{id}")
    public AjaxResult getGoodInfo(@PathVariable("id") Long id)
    {
        return success(sGoodService.selectSGoodById(id));
    }

    /**
     * 获取可升级商品
     * @param sGood
     * @return
     */
    @PostMapping("/getUpgradeGood")
    public List<SGood> getUpgradeGood(SGood sGood){
        List<SGood> list = sGoodService.getUpgradeGood(sGood);
        return list;
    }

}
