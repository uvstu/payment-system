package com.uvstu.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.uvstu.common.annotation.Log;
import com.uvstu.common.core.controller.BaseController;
import com.uvstu.common.core.domain.AjaxResult;
import com.uvstu.common.enums.BusinessType;
import com.uvstu.system.domain.SApplication;
import com.uvstu.system.service.ISApplicationService;
import com.uvstu.common.utils.poi.ExcelUtil;
import com.uvstu.common.core.page.TableDataInfo;

/**
 * 应用管理Controller
 * 
 * @author 郑伟滨
 * @date 2023-02-05
 */
@RestController
@RequestMapping("/system/application")
public class SApplicationController extends BaseController
{
    @Autowired
    private ISApplicationService sApplicationService;

    /**
     * 查询应用管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:list')")
    @GetMapping("/list")
    public TableDataInfo list(SApplication sApplication)
    {
        startPage();
        List<SApplication> list = sApplicationService.selectSApplicationList(sApplication);
        return getDataTable(list);
    }

    /**
     * 导出应用管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:export')")
    @Log(title = "应用管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SApplication sApplication)
    {
        List<SApplication> list = sApplicationService.selectSApplicationList(sApplication);
        ExcelUtil<SApplication> util = new ExcelUtil<SApplication>(SApplication.class);
        util.exportExcel(response, list, "应用管理数据");
    }

    /**
     * 获取应用管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:application:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sApplicationService.selectSApplicationById(id));
    }

    /**
     * 新增应用管理
     */
    @PreAuthorize("@ss.hasPermi('system:application:add')")
    @Log(title = "应用管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SApplication sApplication)
    {
        return toAjax(sApplicationService.insertSApplication(sApplication));
    }

    /**
     * 修改应用管理
     */
    @PreAuthorize("@ss.hasPermi('system:application:edit')")
    @Log(title = "应用管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SApplication sApplication)
    {
        return toAjax(sApplicationService.updateSApplication(sApplication));
    }

    /**
     * 删除应用管理
     */
    @PreAuthorize("@ss.hasPermi('system:application:remove')")
    @Log(title = "应用管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sApplicationService.deleteSApplicationByIds(ids));
    }

    /**
     * 修改应用管理
     */
    @PreAuthorize("@ss.hasPermi('system:application:edit')")
    @Log(title = "应用管理", businessType = BusinessType.UPDATE)
    @RequestMapping("/updateApplicationKey")
    public AjaxResult updateApplicationKey(@RequestBody SApplication sApplication)
    {
        return success(sApplicationService.updateApplicationKey(sApplication));
    }
}
