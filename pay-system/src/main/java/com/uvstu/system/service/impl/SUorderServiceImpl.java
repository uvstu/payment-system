package com.uvstu.system.service.impl;

import java.util.List;
import com.uvstu.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uvstu.system.mapper.SUorderMapper;
import com.uvstu.system.domain.SUorder;
import com.uvstu.system.service.ISUorderService;

/**
 * 用户订单Service业务层处理
 * 
 * @author zwb
 * @date 2023-02-07
 */
@Service
public class SUorderServiceImpl implements ISUorderService 
{
    @Autowired
    private SUorderMapper sUorderMapper;

    /**
     * 查询用户订单
     * 
     * @param id 用户订单主键
     * @return 用户订单
     */
    @Override
    public SUorder selectSUorderById(Long id)
    {
        return sUorderMapper.selectSUorderById(id);
    }

    /**
     * 查询用户订单列表
     * 
     * @param sUorder 用户订单
     * @return 用户订单
     */
    @Override
    public List<SUorder> selectSUorderList(SUorder sUorder)
    {
        return sUorderMapper.selectSUorderList(sUorder);
    }

    /**
     * 新增用户订单
     * 
     * @param sUorder 用户订单
     * @return 结果
     */
    @Override
    public int insertSUorder(SUorder sUorder)
    {
        sUorder.setCreateTime(DateUtils.getNowDate());
        return sUorderMapper.insertSUorder(sUorder);
    }

    /**
     * 修改用户订单
     * 
     * @param sUorder 用户订单
     * @return 结果
     */
    @Override
    public int updateSUorder(SUorder sUorder)
    {
        return sUorderMapper.updateSUorder(sUorder);
    }

    /**
     * 批量删除用户订单
     * 
     * @param ids 需要删除的用户订单主键
     * @return 结果
     */
    @Override
    public int deleteSUorderByIds(Long[] ids)
    {
        return sUorderMapper.deleteSUorderByIds(ids);
    }

    /**
     * 删除用户订单信息
     * 
     * @param id 用户订单主键
     * @return 结果
     */
    @Override
    public int deleteSUorderById(Long id)
    {
        return sUorderMapper.deleteSUorderById(id);
    }

    /**
     * 用户订单结果更新
     * @param sUorder
     */
    @Override
    public int updateSUorderResult(SUorder sUorder) {
        return sUorderMapper.updateSUorderResult(sUorder);
    }

    /**
     * 查询用户订单
     * @param sUorder
     * @return
     */
    @Override
    public SUorder selectSUorderByOutTradeNo(SUorder sUorder) {
        return sUorderMapper.selectSUorderByOutTradeNo(sUorder);
    }

    /**
     * 查询订单数据
     * @param sUorder
     * @return
     */
    @Override
    public List<SUorder> selectOrderData(SUorder sUorder) {
        return sUorderMapper.selectOrderData(sUorder);
    }

    /**
     * 查询支付超时的订单
     * @return
     */
    @Override
    public List<SUorder> selectEndTimeOrder() {
        return sUorderMapper.selectEndTimeOrder();
    }

    /**
     * 获取昨日的订单
     * @param sUorder
     * @return
     */
    @Override
    public List<SUorder> selectToDaySUorderList(SUorder sUorder) {
        return sUorderMapper.selectToDaySUorderList(sUorder);
    }

    /**
     * 获取所有的订单
     * @param sUorder
     * @return
     */
    @Override
    public List<SUorder> selectAllSUorderList(SUorder sUorder) {
        return sUorderMapper.selectAllSUorderList(sUorder);
    }

    /**
     * 获取本月的订单
     * @param sUorder
     * @return
     */
    @Override
    public List<SUorder> selectMonthSUorderList(SUorder sUorder) {
        return sUorderMapper.selectMonthSUorderList(sUorder);
    }

    /**
     * 获取本日订单
     * @param sUorder
     * @return
     */
    @Override
    public List<SUorder> selectTodaySUorderList(SUorder sUorder) {
        return sUorderMapper.selectTodaySUorderList(sUorder);
    }
}
