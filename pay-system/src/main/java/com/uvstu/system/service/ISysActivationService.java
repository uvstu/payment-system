package com.uvstu.system.service;

import java.util.Map;

public interface ISysActivationService {

    /**
     * 验证码生成
     */
    void init();

    /**
     * 获取验证码
     * @return
     */
    String getGkey();

    /**
     * 激活
     * @param gKey
     * @param aKey
     * @return
     */
    boolean activation(String gKey,String aKey);

    /**
     * 校验平台是否激活
     * @return
     */
    boolean checkVerifyKey();

    /**
     * 获取平台激活状态
     * @return
     */
    Map<String, Object> checkKeyData();
}
