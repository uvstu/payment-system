package com.uvstu.system.service;

import java.util.List;
import com.uvstu.system.domain.SStatistics;

/**
 * 用户订单统计Service接口
 * 
 * @author zwb
 * @date 2023-02-09
 */
public interface ISStatisticsService 
{
    /**
     * 查询用户订单统计
     * 
     * @param id 用户订单统计主键
     * @return 用户订单统计
     */
    public SStatistics selectSStatisticsById(Long id);

    /**
     * 查询用户订单统计列表
     * 
     * @param sStatistics 用户订单统计
     * @return 用户订单统计集合
     */
    public List<SStatistics> selectSStatisticsList(SStatistics sStatistics);

    /**
     * 新增用户订单统计
     * 
     * @param sStatistics 用户订单统计
     * @return 结果
     */
    public int insertSStatistics(SStatistics sStatistics);

    /**
     * 修改用户订单统计
     * 
     * @param sStatistics 用户订单统计
     * @return 结果
     */
    public int updateSStatistics(SStatistics sStatistics);

    /**
     * 批量删除用户订单统计
     * 
     * @param ids 需要删除的用户订单统计主键集合
     * @return 结果
     */
    public int deleteSStatisticsByIds(Long[] ids);

    /**
     * 删除用户订单统计信息
     * 
     * @param id 用户订单统计主键
     * @return 结果
     */
    public int deleteSStatisticsById(Long id);

    /**
     * 根据 appId 和 userId 查询数据
     * @param sStatistics
     * @return
     */
    SStatistics selectSStatisticsByAppid(SStatistics sStatistics);

    /**
     * 获取数据
     * @param sStatistics
     * @return
     */
    SStatistics getAppIdStatistics(SStatistics sStatistics);
}
