package com.uvstu.system.service.impl;

import com.uvstu.common.config.RuoYiConfig;
import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.pay.client.AliPayClient;
import com.uvstu.common.pay.client.WxPayClient;
import com.uvstu.common.utils.DateUtils;
import com.uvstu.common.utils.SecurityUtils;
import com.uvstu.system.domain.SPayConfig;
import com.uvstu.system.mapper.SPayConfigMapper;
import com.uvstu.system.service.ISPayConfigService;
import com.uvstu.system.service.ISysConfigService;
import com.uvstu.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付配置Service业务层处理
 * 
 * @author zwb
 * @date 2022-10-05
 */
@Service
public class SPayConfigServiceImpl implements ISPayConfigService
{
    @Autowired
    private SPayConfigMapper sPayConfigMapper;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private ISysDictDataService dictDataService;

    /**
     * 查询支付配置
     * 
     * @param id 支付配置主键
     * @return 支付配置
     */
    @Override
    public SPayConfig selectSPayConfigById(Long id)
    {
        return sPayConfigMapper.selectSPayConfigById(id);
    }

    /**
     * 查询支付配置列表
     * 
     * @param sPayConfig 支付配置
     * @return 支付配置
     */
    @Override
    public List<SPayConfig> selectSPayConfigList(SPayConfig sPayConfig)
    {
        return sPayConfigMapper.selectSPayConfigList(sPayConfig);
    }

    /**
     * 新增支付配置
     * 
     * @param sPayConfig 支付配置
     * @return 结果
     */
    @Override
    public int insertSPayConfig(SPayConfig sPayConfig)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sPayConfig.setCreateBy(user.getUserName());
        sPayConfig.setCreateTime(DateUtils.getNowDate());
        sPayConfig.setUpdateBy(user.getUserName());
        sPayConfig.setUpdateTime(DateUtils.getNowDate());
        int result = sPayConfigMapper.insertSPayConfig(sPayConfig);
        init();
        return result;
    }

    /**
     * 修改支付配置
     * 
     * @param sPayConfig 支付配置
     * @return 结果
     */
    @Override
    public int updateSPayConfig(SPayConfig sPayConfig)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sPayConfig.setUpdateBy(user.getUserName());
        sPayConfig.setUpdateTime(DateUtils.getNowDate());
        int result =  sPayConfigMapper.updateSPayConfig(sPayConfig);
        init();
        return result;
    }

    /**
     * 批量删除支付配置
     * 
     * @param ids 需要删除的支付配置主键
     * @return 结果
     */
    @Override
    public int deleteSPayConfigByIds(Long[] ids)
    {
        return sPayConfigMapper.deleteSPayConfigByIds(ids);
    }

    /**
     * 删除支付配置信息
     * 
     * @param id 支付配置主键
     * @return 结果
     */
    @Override
    public int deleteSPayConfigById(Long id)
    {
        return sPayConfigMapper.deleteSPayConfigById(id);
    }

    @Override
    public void init() {
        SPayConfig sPayConfig = new SPayConfig();
        sPayConfig.setPayStatus("2");
        sPayConfig.setPaymentChannel("1");
        List<SPayConfig> configs = selectSPayConfigList(sPayConfig);
        if (configs.size() != 0) {
            sPayConfig = configs.get(0);
            //请求第三方扫码支付接口
            String notifyUrl = configService.selectConfigByKey("sys.url") + "/open/pay/api/aliPayNotify";
            String returnUrl = configService.selectConfigByKey("web.url") + "/muser/sorder";
            try {
                AliPayClient.setAliPayConfig(sPayConfig.getGatewayAddress(), sPayConfig.getAppid(),
                        sPayConfig.getKey1(), sPayConfig.getKey2(), dictDataService.selectDictLabel("sign_type", sPayConfig.getSignType()),
                        sPayConfig.getDataFormat(), dictDataService.selectDictLabel("coding_set", sPayConfig.getCodingSet()), notifyUrl, returnUrl);
            } catch (Exception e) {
                //支付宝初始化失败
                e.printStackTrace();
            }
        }
        sPayConfig = new SPayConfig();
        sPayConfig.setPayStatus("2");
        sPayConfig.setPaymentChannel("2");
        configs = selectSPayConfigList(sPayConfig);
        if (configs.size() != 0) {
            sPayConfig = configs.get(0);
            String payNotifyUrl = configService.selectConfigByKey("sys.url")+"/open/pay/api/wxPayNotify";
            String refundNotifyUrl = configService.selectConfigByKey("sys.url")+"/open/pay/api/wxRefundNotify";
            String[] arr = sPayConfig.getCertificatePath().split(",");
            String certificatePath = "";
            for (int i = 0; i < arr.length; i++) {
                if(arr[i].indexOf("p12") != -1){
                    certificatePath = RuoYiConfig.getProfile() + arr[i].replace("/profile/","/");
                }
            }
            try{
                WxPayClient.setWxPayConfig(sPayConfig.getAppid(),sPayConfig.getKey2(),sPayConfig.getKey1(),payNotifyUrl,refundNotifyUrl,
                        certificatePath,sPayConfig.getKey1());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}
