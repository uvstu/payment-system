package com.uvstu.system.service;

import java.util.List;
import com.uvstu.system.domain.SOrder;

/**
 * 订单Service接口
 * 
 * @author 郑伟滨
 * @date 2023-02-01
 */
public interface ISOrderService 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public SOrder selectSOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param sOrder 订单
     * @return 订单集合
     */
    public List<SOrder> selectSOrderList(SOrder sOrder);

    /**
     * 新增订单
     * 
     * @param sOrder 订单
     * @return 结果
     */
    public int insertSOrder(SOrder sOrder);

    /**
     * 修改订单
     * 
     * @param sOrder 订单
     * @return 结果
     */
    public int updateSOrder(SOrder sOrder);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteSOrderByIds(Long[] ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteSOrderById(Long id);

    /**
     * 订单支付成功
     * @param sOrder
     */
    void paySuccess(SOrder sOrder);

    /**
     * 查询订单数据
     * @param sOrder
     * @return
     */
    List<SOrder> selectOrderData(SOrder sOrder);

    /**
     * 查询订单
     * @param sOrder
     * @return
     */
    SOrder selectSOrderByTradeNo(SOrder sOrder);

    /**
     * 查询支付超时的订单
     * @return
     */
    List<SOrder> selectEndTimeOrder();

    /**
     * 查询支付成功的所有订单
     * @return
     */
    List<SOrder> selectAllSuccessOrder();

    /**
     * 查询用户最新可用的订单
     * @param sOrder
     * @return
     */
    List<SOrder> selectUserOrder(SOrder sOrder);
}
