package com.uvstu.system.service.impl;

import java.util.List;

import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.utils.DateUtils;
import com.uvstu.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uvstu.system.mapper.SGoodMapper;
import com.uvstu.system.domain.SGood;
import com.uvstu.system.service.ISGoodService;

/**
 * 商品Service业务层处理
 * 
 * @author zwb
 * @date 2023-02-01
 */
@Service
public class SGoodServiceImpl implements ISGoodService 
{
    @Autowired
    private SGoodMapper sGoodMapper;

    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    @Override
    public SGood selectSGoodById(Long id)
    {
        return sGoodMapper.selectSGoodById(id);
    }

    /**
     * 查询商品列表
     * 
     * @param sGood 商品
     * @return 商品
     */
    @Override
    public List<SGood> selectSGoodList(SGood sGood)
    {
        return sGoodMapper.selectSGoodList(sGood);
    }

    /**
     * 新增商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    @Override
    public int insertSGood(SGood sGood)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sGood.setCreateBy(user.getUserName());
        sGood.setCreateTime(DateUtils.getNowDate());
        sGood.setUpdateBy(user.getUserName());
        sGood.setUpdateTime(DateUtils.getNowDate());
        return sGoodMapper.insertSGood(sGood);
    }

    /**
     * 修改商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    @Override
    public int updateSGood(SGood sGood)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sGood.setUpdateBy(user.getUserName());
        sGood.setUpdateTime(DateUtils.getNowDate());
        return sGoodMapper.updateSGood(sGood);
    }

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品主键
     * @return 结果
     */
    @Override
    public int deleteSGoodByIds(Long[] ids)
    {
        return sGoodMapper.deleteSGoodByIds(ids);
    }

    /**
     * 删除商品信息
     * 
     * @param id 商品主键
     * @return 结果
     */
    @Override
    public int deleteSGoodById(Long id)
    {
        return sGoodMapper.deleteSGoodById(id);
    }

    /**
     * 获取可升级商品
     * @param sGood
     * @return
     */
    @Override
    public List<SGood> getUpgradeGood(SGood sGood) {
        //获取当前商品
        SGood good = sGoodMapper.selectSGoodById(sGood.getId());
        return sGoodMapper.getUpgradeGood(good.getPrice());
    }
}
