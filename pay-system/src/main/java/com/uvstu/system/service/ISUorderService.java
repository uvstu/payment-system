package com.uvstu.system.service;

import java.util.List;
import com.uvstu.system.domain.SUorder;

/**
 * 用户订单Service接口
 * 
 * @author zwb
 * @date 2023-02-07
 */
public interface ISUorderService 
{
    /**
     * 查询用户订单
     * 
     * @param id 用户订单主键
     * @return 用户订单
     */
    public SUorder selectSUorderById(Long id);

    /**
     * 查询用户订单列表
     * 
     * @param sUorder 用户订单
     * @return 用户订单集合
     */
    public List<SUorder> selectSUorderList(SUorder sUorder);

    /**
     * 新增用户订单
     * 
     * @param sUorder 用户订单
     * @return 结果
     */
    public int insertSUorder(SUorder sUorder);

    /**
     * 修改用户订单
     * 
     * @param sUorder 用户订单
     * @return 结果
     */
    public int updateSUorder(SUorder sUorder);

    /**
     * 批量删除用户订单
     * 
     * @param ids 需要删除的用户订单主键集合
     * @return 结果
     */
    public int deleteSUorderByIds(Long[] ids);

    /**
     * 删除用户订单信息
     * 
     * @param id 用户订单主键
     * @return 结果
     */
    public int deleteSUorderById(Long id);

    /**
     * 结果订单更新
     * @param sUorder
     */
    public int updateSUorderResult(SUorder sUorder);

    /**
     * 查询用户订单
     * @param sUorder
     */
    SUorder selectSUorderByOutTradeNo(SUorder sUorder);

    /**
     * 查询订单数据
     * @param sUorder
     * @return
     */
    List<SUorder> selectOrderData(SUorder sUorder);

    /**
     * 查询支付超时的订单
     * @return
     */
    List<SUorder> selectEndTimeOrder();

    /**
     * 获取昨日的订单
     * @param sUorder
     * @return
     */
    List<SUorder> selectToDaySUorderList(SUorder sUorder);

    /**
     * 获取所有的订单
     * @param sUorder
     * @return
     */
    List<SUorder> selectAllSUorderList(SUorder sUorder);

    /**
     * 获取本月的订单
     * @param sUorder
     * @return
     */
    List<SUorder> selectMonthSUorderList(SUorder sUorder);

    /**
     * 获取本日订单
     * @param sUorder
     * @return
     */
    List<SUorder> selectTodaySUorderList(SUorder sUorder);
}
