package com.uvstu.system.service;

import com.uvstu.system.domain.SPayConfig;

import java.util.List;

/**
 * 支付配置Service接口
 * 
 * @author zwb
 * @date 2022-10-05
 */
public interface ISPayConfigService 
{
    /**
     * 查询支付配置
     * 
     * @param id 支付配置主键
     * @return 支付配置
     */
    public SPayConfig selectSPayConfigById(Long id);

    /**
     * 查询支付配置列表
     * 
     * @param sPayConfig 支付配置
     * @return 支付配置集合
     */
    public List<SPayConfig> selectSPayConfigList(SPayConfig sPayConfig);

    /**
     * 新增支付配置
     * 
     * @param sPayConfig 支付配置
     * @return 结果
     */
    public int insertSPayConfig(SPayConfig sPayConfig);

    /**
     * 修改支付配置
     * 
     * @param sPayConfig 支付配置
     * @return 结果
     */
    public int updateSPayConfig(SPayConfig sPayConfig);

    /**
     * 批量删除支付配置
     * 
     * @param ids 需要删除的支付配置主键集合
     * @return 结果
     */
    public int deleteSPayConfigByIds(Long[] ids);

    /**
     * 删除支付配置信息
     * 
     * @param id 支付配置主键
     * @return 结果
     */
    public int deleteSPayConfigById(Long id);

    /**
     * 初始化配置
     */
    void init();
}
