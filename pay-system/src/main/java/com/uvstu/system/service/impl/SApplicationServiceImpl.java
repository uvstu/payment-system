package com.uvstu.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.utils.DateUtils;
import com.uvstu.common.utils.SecurityUtils;
import com.uvstu.system.domain.SGood;
import com.uvstu.system.domain.SOrder;
import com.uvstu.system.service.ISGoodService;
import com.uvstu.system.service.ISOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uvstu.system.mapper.SApplicationMapper;
import com.uvstu.system.domain.SApplication;
import com.uvstu.system.service.ISApplicationService;

/**
 * 应用管理Service业务层处理
 * 
 * @author 郑伟滨
 * @date 2023-02-05
 */
@Service
public class SApplicationServiceImpl implements ISApplicationService 
{
    @Autowired
    private SApplicationMapper sApplicationMapper;

    @Autowired
    private ISOrderService sOrderService;

    @Autowired
    private ISGoodService sGoodService;

    /**
     * 查询应用管理
     * 
     * @param id 应用管理主键
     * @return 应用管理
     */
    @Override
    public SApplication selectSApplicationById(Long id)
    {
        return sApplicationMapper.selectSApplicationById(id);
    }

    /**
     * 查询应用管理列表
     * 
     * @param sApplication 应用管理
     * @return 应用管理
     */
    @Override
    public List<SApplication> selectSApplicationList(SApplication sApplication)
    {
        if(sApplication.getUserId() == null){
            SysUser user = SecurityUtils.getLoginUser().getUser();
            sApplication.setUserId(user.getUserId());
        }
        return sApplicationMapper.selectSApplicationList(sApplication);
    }

    /**
     * 新增应用管理
     * 
     * @param sApplication 应用管理
     * @return 结果
     */
    @Override
    public int insertSApplication(SApplication sApplication)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        //校验是否存在订单
        SOrder sOrder = new SOrder();
        sOrder.setUserId(user.getUserId());
        sOrder.setStatus(2);
        List<SOrder> orderlist = sOrderService.selectSOrderList(sOrder);
        if(orderlist.size() == 0) {
            return 0;
        }
        //校验应用数量是否超额
        SApplication oldSApplication = new SApplication();
        oldSApplication.setUserId(user.getUserId());
        List<SApplication> applist = sApplicationMapper.selectSApplicationList(oldSApplication);
        if(applist.size() != 0){
            if(orderlist.get(0).getAppNum() <= applist.size()){
                return 0;
            }
        }
        sApplication.setPayMethod(Integer.valueOf(orderlist.get(0).getPayChannel()));
        sApplication.setCreateBy(user.getUserName());
        sApplication.setCreateTime(DateUtils.getNowDate());
        sApplication.setUpdateBy(user.getUserName());
        sApplication.setUpdateTime(DateUtils.getNowDate());
        sApplication.setUserId(user.getUserId());
        //生成应用ID
        sApplication.setAppId(String.valueOf(System.currentTimeMillis()));
        //生成签名key
        sApplication.setSignKey(UUID.randomUUID().toString().replaceAll("-",""));
        //支付渠道
        return sApplicationMapper.insertSApplication(sApplication);
    }

    /**
     * 修改应用管理
     * 
     * @param sApplication 应用管理
     * @return 结果
     */
    @Override
    public int updateSApplication(SApplication sApplication)
    {
        SApplication old = sApplicationMapper.selectSApplicationById(sApplication.getId());
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sApplication.setUpdateBy(user.getUserName());
        sApplication.setUpdateTime(DateUtils.getNowDate());
        if(old.getStatus() != 2 && sApplication.getStatus() == 2){
            //校验是否存在订单
            SOrder sOrder = new SOrder();
            sOrder.setUserId(user.getUserId());
            sOrder.setStatus(2);
            List<SOrder> orderlist = sOrderService.selectSOrderList(sOrder);
            if(orderlist.size() == 0) {
                return 0;
            }
            //判断应用是否超额
            SApplication oldSApplication = new SApplication();
            oldSApplication.setUserId(user.getUserId());
            oldSApplication.setStatus(2);
            List<SApplication> applist = sApplicationMapper.selectSApplicationList(oldSApplication);
            if(applist.size() != 0){
                if(orderlist.get(0).getAppNum() <= applist.size()){
                    return 0;
                }
            }
        }
        return sApplicationMapper.updateSApplication(sApplication);
    }

    /**
     * 批量删除应用管理
     * 
     * @param ids 需要删除的应用管理主键
     * @return 结果
     */
    @Override
    public int deleteSApplicationByIds(Long[] ids)
    {
        return sApplicationMapper.deleteSApplicationByIds(ids);
    }

    /**
     * 删除应用管理信息
     * 
     * @param id 应用管理主键
     * @return 结果
     */
    @Override
    public int deleteSApplicationById(Long id)
    {
        return sApplicationMapper.deleteSApplicationById(id);
    }

    /**
     * 更新用户应用状态
     * @param sApplication
     */
    @Override
    public void updateSApplicationStatus(SApplication sApplication) {
        sApplicationMapper.updateSApplicationStatus(sApplication);
    }

    /**
     * 查询应用
     * @param appId
     * @return
     */
    @Override
    public SApplication selectSApplicationByAppid(String appId) {
        SApplication sApplication = new SApplication();
        sApplication.setAppId(appId);
        List<SApplication> s = sApplicationMapper.selectSApplicationList(sApplication);
        if(s.size() != 0){
            return s.get(0);
        }
        return null;
    }

    /**
     * 更新指定用户所有应用支付通道
     * @param sApplication
     */
    @Override
    public void updateSApplicationPayMethod(SApplication sApplication) {
        sApplicationMapper.updateSApplicationPayMethod(sApplication);
    }

    /**
     * 查询所有的应用
     * @return
     */
    @Override
    public List<SApplication> selectAllSApplication() {
        return sApplicationMapper.selectAllSApplication();
    }

    /**
     * 更换应用key
     * @param sApplication
     * @return
     */
    @Override
    public SApplication updateApplicationKey(SApplication sApplication) {
        //生成签名key
        sApplication.setSignKey(UUID.randomUUID().toString().replaceAll("-",""));
        sApplicationMapper.updateSApplication(sApplication);
        return sApplication;
    }
}
