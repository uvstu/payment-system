package com.uvstu.system.service.impl;

import java.util.List;

import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.utils.DateUtils;
import com.uvstu.common.utils.SecurityUtils;
import com.uvstu.system.domain.SApplication;
import com.uvstu.system.service.ISApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uvstu.system.mapper.SOrderMapper;
import com.uvstu.system.domain.SOrder;
import com.uvstu.system.service.ISOrderService;

/**
 * 订单Service业务层处理
 * 
 * @author 郑伟滨
 * @date 2023-02-01
 */
@Service
public class SOrderServiceImpl implements ISOrderService 
{
    @Autowired
    private SOrderMapper sOrderMapper;

    @Autowired
    private ISApplicationService isApplicationService;

    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public SOrder selectSOrderById(Long id)
    {
        return sOrderMapper.selectSOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param sOrder 订单
     * @return 订单
     */
    @Override
    public List<SOrder> selectSOrderList(SOrder sOrder)
    {
        return sOrderMapper.selectSOrderList(sOrder);
    }

    /**
     * 新增订单
     * 
     * @param sOrder 订单
     * @return 结果
     */
    @Override
    public int insertSOrder(SOrder sOrder)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sOrder.setCreateBy(user.getUserName());
        sOrder.setCreateTime(DateUtils.getNowDate());
        return sOrderMapper.insertSOrder(sOrder);
    }

    /**
     * 修改订单
     * 
     * @param sOrder 订单
     * @return 结果
     */
    @Override
    public int updateSOrder(SOrder sOrder)
    {
        //用户应用全部停用
        SApplication sApplication = new SApplication();
        sApplication.setUserId(sOrder.getUserId());
        sApplication.setStatus(1);
        sApplication.setPayMethod(Integer.valueOf(sOrder.getPayChannel()));
        isApplicationService.updateSApplicationStatus(sApplication);
        isApplicationService.updateSApplicationPayMethod(sApplication);
        return sOrderMapper.updateSOrder(sOrder);
    }

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Override
    public int deleteSOrderByIds(Long[] ids)
    {
        return sOrderMapper.deleteSOrderByIds(ids);
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    @Override
    public int deleteSOrderById(Long id)
    {
        return sOrderMapper.deleteSOrderById(id);
    }

    @Override
    public void paySuccess(SOrder sOrder) {
        try{
            sOrderMapper.paySuccess(sOrder);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 查询订单数据
     * @param sOrder
     * @return
     */
    @Override
    public List<SOrder> selectOrderData(SOrder sOrder) {
        return sOrderMapper.selectOrderData(sOrder);
    }

    /**
     * 查询订单
     * @param sOrder
     * @return
     */
    @Override
    public SOrder selectSOrderByTradeNo(SOrder sOrder) {
        return sOrderMapper.selectSOrderByTradeNo(sOrder);
    }

    /**
     * 查询超时的订单
     * @return
     */
    @Override
    public List<SOrder> selectEndTimeOrder() {
        return sOrderMapper.selectEndTimeOrder();
    }

    /**
     * 查询支付成功的所有订单
     * @return
     */
    @Override
    public List<SOrder> selectAllSuccessOrder() {
        return sOrderMapper.selectAllSuccessOrder();
    }

    /**
     * 查询用户最新可用的订单
     * @param sOrder
     * @return
     */
    @Override
    public List<SOrder> selectUserOrder(SOrder sOrder) {
        return sOrderMapper.selectUserOrder(sOrder);
    }
}
