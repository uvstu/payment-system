package com.uvstu.system.service;

import java.util.List;
import com.uvstu.system.domain.SGood;

/**
 * 商品Service接口
 * 
 * @author zwb
 * @date 2023-02-01
 */
public interface ISGoodService 
{
    /**
     * 查询商品
     * 
     * @param id 商品主键
     * @return 商品
     */
    public SGood selectSGoodById(Long id);

    /**
     * 查询商品列表
     * 
     * @param sGood 商品
     * @return 商品集合
     */
    public List<SGood> selectSGoodList(SGood sGood);

    /**
     * 新增商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    public int insertSGood(SGood sGood);

    /**
     * 修改商品
     * 
     * @param sGood 商品
     * @return 结果
     */
    public int updateSGood(SGood sGood);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的商品主键集合
     * @return 结果
     */
    public int deleteSGoodByIds(Long[] ids);

    /**
     * 删除商品信息
     * 
     * @param id 商品主键
     * @return 结果
     */
    public int deleteSGoodById(Long id);

    /**
     * 获取可升级商品
     * @param sGood
     * @return
     */
    List<SGood> getUpgradeGood(SGood sGood);
}
