package com.uvstu.system.service.impl;

import java.util.List;

import com.uvstu.common.core.domain.entity.SysUser;
import com.uvstu.common.utils.DateUtils;
import com.uvstu.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.uvstu.system.mapper.SStatisticsMapper;
import com.uvstu.system.domain.SStatistics;
import com.uvstu.system.service.ISStatisticsService;

/**
 * 用户订单统计Service业务层处理
 * 
 * @author zwb
 * @date 2023-02-09
 */
@Service
public class SStatisticsServiceImpl implements ISStatisticsService 
{
    @Autowired
    private SStatisticsMapper sStatisticsMapper;

    /**
     * 查询用户订单统计
     * 
     * @param id 用户订单统计主键
     * @return 用户订单统计
     */
    @Override
    public SStatistics selectSStatisticsById(Long id)
    {
        return sStatisticsMapper.selectSStatisticsById(id);
    }

    /**
     * 查询用户订单统计列表
     * 
     * @param sStatistics 用户订单统计
     * @return 用户订单统计
     */
    @Override
    public List<SStatistics> selectSStatisticsList(SStatistics sStatistics)
    {
        return sStatisticsMapper.selectSStatisticsList(sStatistics);
    }

    /**
     * 新增用户订单统计
     * 
     * @param sStatistics 用户订单统计
     * @return 结果
     */
    @Override
    public int insertSStatistics(SStatistics sStatistics)
    {
        sStatistics.setCreateBy("system");
        sStatistics.setCreateTime(DateUtils.getNowDate());
        return sStatisticsMapper.insertSStatistics(sStatistics);
    }

    /**
     * 修改用户订单统计
     * 
     * @param sStatistics 用户订单统计
     * @return 结果
     */
    @Override
    public int updateSStatistics(SStatistics sStatistics)
    {
        sStatistics.setUpdateBy("system");
        sStatistics.setUpdateTime(DateUtils.getNowDate());
        return sStatisticsMapper.updateSStatistics(sStatistics);
    }

    /**
     * 批量删除用户订单统计
     * 
     * @param ids 需要删除的用户订单统计主键
     * @return 结果
     */
    @Override
    public int deleteSStatisticsByIds(Long[] ids)
    {
        return sStatisticsMapper.deleteSStatisticsByIds(ids);
    }

    /**
     * 删除用户订单统计信息
     * 
     * @param id 用户订单统计主键
     * @return 结果
     */
    @Override
    public int deleteSStatisticsById(Long id)
    {
        return sStatisticsMapper.deleteSStatisticsById(id);
    }

    /**
     * 根据 appId 和 userId 查询数据
     * @param sStatistics
     * @return
     */
    @Override
    public SStatistics selectSStatisticsByAppid(SStatistics sStatistics) {
        return sStatisticsMapper.selectSStatisticsByAppid(sStatistics);
    }

    /**
     * 获取统计数据
     * @param sStatistics
     * @return
     */
    @Override
    public SStatistics getAppIdStatistics(SStatistics sStatistics) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sStatistics.setUserid(user.getUserId());
        return sStatisticsMapper.selectSStatisticsByAppid(sStatistics);
    }
}
