package com.uvstu.system.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.uvstu.common.utils.MacUtils;
import com.uvstu.common.utils.VerifyKeyUtils;
import com.uvstu.common.utils.sign.RSAUtils;
import com.uvstu.system.mapper.SysActivationMapper;
import com.uvstu.system.service.ISysActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class SysActivationServiceImpl implements ISysActivationService {

    @Autowired
    private SysActivationMapper sysActivationMapper;

    /**
     * 初始化验证码
     */
    @Override
    public void init() {

    }

    /**
     * 获取验证码
     * @return
     */
    @Override
    public String getGkey() {
        String mac = getMacAddress();
        if(mac != null){
            //公钥加密
            String gkey = RSAUtils.encryptByPublicKey(mac);
            return gkey;
        }
        return null;
    }

    /**
     * 激活
     * @param gKey
     * @param aKey
     * @return
     */
    @Override
    public boolean activation(String gKey,String aKey) {
        try{
            //验证码是否匹配
            boolean result = VerifyKeyUtils.verifyKey(aKey);
            if(result){
                sysActivationMapper.delAll();
                sysActivationMapper.insertActivation(gKey,aKey);
                return true;
            }
            return  false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 校验平台是否激活
     * @return
     */
    @Override
    public boolean checkVerifyKey() {
        try{
            //查询激活码
            Map<String,String> map = sysActivationMapper.selectActivationData();
            if(map == null){
                return false;
            }
            String aKey = map.get("akey");
            //校验数据
            boolean result = VerifyKeyUtils.verifyKey(aKey);
            if(result){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取平台激活状态
     * @return
     */
    @Override
    public Map<String, Object> checkKeyData() {
        Map<String,Object> map = new HashMap<>();
        boolean result = false;
        String time = "--";
        try{
            //查询激活码
            Map<String,String> data = sysActivationMapper.selectActivationData();
            if(map == null){
                result = false;
            }else{
                String aKey = data.get("akey");
                //校验数据
                result = VerifyKeyUtils.verifyKey(aKey);
                if(result){
                    //获取时间
                    time = VerifyKeyUtils.getEndTime(aKey);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        map.put("result",result);
        map.put("endTime",time);
        return map;
    }

    /**
     * 客户端生成验证秘钥
     */
    private static String getMacAddress(){
        String mac = null;
        try{
            if(MacUtils.getMAC() !=null){
                mac = MacUtils.getMAC();
            }else if(MacUtils.getMacAddress().size() != 0){
                mac = JSONObject.toJSONString(MacUtils.getMacAddress());
            }else if(MacUtils.getLinuxMACAddress().size() != 0){
                mac = JSONObject.toJSONString(MacUtils.getLinuxMACAddress());
            }else if(MacUtils.getMACAddressByWindows().size() != 0){
                mac = JSONObject.toJSONString(MacUtils.getLinuxMACAddress());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return mac;
    }



}
