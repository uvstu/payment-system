package com.uvstu.system.domain;

import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 支付配置对象 s_pay_config
 * 
 * @author zwb
 * @date 2022-10-05
 */
public class SPayConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private String paymentChannel;

    /** 网关地址 */
    @Excel(name = "网关地址")
    private String gatewayAddress;

    /** 签名类型 */
    @Excel(name = "签名类型")
    private String signType;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appid;

    /** 密钥1 */
    @Excel(name = "支付宝公钥/微信商户号")
    private String key1;

    /** 密钥2 */
    @Excel(name = "支付宝私钥/微信APIKEY")
    private String key2;

    /** 数据格式 */
    @Excel(name = "数据格式")
    private String dataFormat;

    /** 编码集 */
    @Excel(name = "编码集")
    private String codingSet;

    /** 证书路径 */
    @Excel(name = "证书路径")
    private String certificatePath;

    /** 证书密码 */
    @Excel(name = "证书密码")
    private String certificatePassword;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private String payStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setPaymentChannel(String paymentChannel) 
    {
        this.paymentChannel = paymentChannel;
    }

    public String getPaymentChannel() 
    {
        return paymentChannel;
    }
    public void setGatewayAddress(String gatewayAddress) 
    {
        this.gatewayAddress = gatewayAddress;
    }

    public String getGatewayAddress() 
    {
        return gatewayAddress;
    }
    public void setSignType(String signType) 
    {
        this.signType = signType;
    }

    public String getSignType() 
    {
        return signType;
    }
    public void setAppid(String appid) 
    {
        this.appid = appid;
    }

    public String getAppid() 
    {
        return appid;
    }
    public void setKey1(String key1) 
    {
        this.key1 = key1;
    }

    public String getKey1() 
    {
        return key1;
    }
    public void setKey2(String key2) 
    {
        this.key2 = key2;
    }

    public String getKey2() 
    {
        return key2;
    }
    public void setDataFormat(String dataFormat) 
    {
        this.dataFormat = dataFormat;
    }

    public String getDataFormat() 
    {
        return dataFormat;
    }
    public void setCodingSet(String codingSet) 
    {
        this.codingSet = codingSet;
    }

    public String getCodingSet() 
    {
        return codingSet;
    }
    public void setPayStatus(String payStatus) 
    {
        this.payStatus = payStatus;
    }

    public String getPayStatus() 
    {
        return payStatus;
    }

    public String getCertificatePath() {
        return certificatePath;
    }

    public void setCertificatePath(String certificatePath) {
        this.certificatePath = certificatePath;
    }

    public String getCertificatePassword() {
        return certificatePassword;
    }

    public void setCertificatePassword(String certificatePassword) {
        this.certificatePassword = certificatePassword;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productName", getProductName())
            .append("paymentChannel", getPaymentChannel())
            .append("gatewayAddress", getGatewayAddress())
            .append("signType", getSignType())
            .append("appid", getAppid())
            .append("key1", getKey1())
            .append("key2", getKey2())
            .append("dataFormat", getDataFormat())
            .append("codingSet", getCodingSet())
            .append("payStatus", getPayStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("certificatePath", getCertificatePath())
            .append("certificatePassword", getCertificatePassword())
            .toString();
    }
}
