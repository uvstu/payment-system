package com.uvstu.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 用户订单统计对象 statistics
 * 
 * @author zwb
 * @date 2023-02-09
 */
public class SStatistics extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 统计类型 1-支付 2-退款 */
    @Excel(name = "统计类型 1-支付 2-退款")
    private Long type;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userid;

    /** 应用ID */
    @Excel(name = "应用ID")
    private Long appid;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal totalAmount;

    /** 今日金额 */
    @Excel(name = "今日金额")
    private BigDecimal todayAmount;

    /** 昨日金额 */
    @Excel(name = "昨日金额")
    private BigDecimal yesterdayAmount;

    /** 本月金额 */
    @Excel(name = "本月金额")
    private BigDecimal monthAmount;

    /** 近x天金额 */
    @Excel(name = "近x天金额")
    private String nearlyXDays;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setUserid(Long userid) 
    {
        this.userid = userid;
    }

    public Long getUserid() 
    {
        return userid;
    }
    public void setAppid(Long appid) 
    {
        this.appid = appid;
    }

    public Long getAppid() 
    {
        return appid;
    }
    public void setTotalAmount(BigDecimal totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() 
    {
        return totalAmount;
    }
    public void setTodayAmount(BigDecimal todayAmount) 
    {
        this.todayAmount = todayAmount;
    }

    public BigDecimal getTodayAmount() 
    {
        return todayAmount;
    }
    public void setYesterdayAmount(BigDecimal yesterdayAmount) 
    {
        this.yesterdayAmount = yesterdayAmount;
    }

    public BigDecimal getYesterdayAmount() 
    {
        return yesterdayAmount;
    }
    public void setMonthAmount(BigDecimal monthAmount) 
    {
        this.monthAmount = monthAmount;
    }

    public BigDecimal getMonthAmount() 
    {
        return monthAmount;
    }
    public void setNearlyXDays(String nearlyXDays) 
    {
        this.nearlyXDays = nearlyXDays;
    }

    public String getNearlyXDays() 
    {
        return nearlyXDays;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("userid", getUserid())
            .append("appid", getAppid())
            .append("totalAmount", getTotalAmount())
            .append("todayAmount", getTodayAmount())
            .append("yesterdayAmount", getYesterdayAmount())
            .append("monthAmount", getMonthAmount())
            .append("nearlyXDays", getNearlyXDays())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .toString();
    }
}
