package com.uvstu.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 订单对象 s_order
 * 
 * @author 郑伟滨
 * @date 2023-02-01
 */
public class SOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 平台订单号 */
    @Excel(name = "平台订单号")
    private String orderId;

    /** 第三方订单号 */
    @Excel(name = "第三方订单号")
    private String tradeNo;

    /** 第三方退款单号 */
    @Excel(name = "第三方退款单号")
    private String refundId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long goodId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private Integer payMethod;

    /** 应用数量 */
    @Excel(name = "应用数量")
    private Integer appNum;

    /** 付款金额 */
    @Excel(name = "付款金额")
    private BigDecimal price;

    /** 到期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 支持渠道 */
    @Excel(name = "支持渠道")
    private String payChannel;

    /** 费率 */
    @Excel(name = "费率")
    private BigDecimal rate;

    /** 订单状态 1-未支付 2-支付成功 3-支付失败 */
    @Excel(name = "订单状态 1-未支付 2-支付成功 3-支付失败")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setTradeNo(String tradeNo) 
    {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() 
    {
        return tradeNo;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setPayMethod(Integer payMethod) 
    {
        this.payMethod = payMethod;
    }

    public Integer getPayMethod() 
    {
        return payMethod;
    }
    public Integer getAppNum() {
        return appNum;
    }

    public void setAppNum(Integer appNum) {
        this.appNum = appNum;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setPayChannel(String payChannel) 
    {
        this.payChannel = payChannel;
    }

    public String getPayChannel() 
    {
        return payChannel;
    }
    public void setRate(BigDecimal rate) 
    {
        this.rate = rate;
    }

    public BigDecimal getRate() 
    {
        return rate;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("tradeNo", getTradeNo())
            .append("refundId", getRefundId())
            .append("userId", getUserId())
            .append("goodId", getGoodId())
            .append("goodName", getGoodName())
            .append("payMethod", getPayMethod())
            .append("appNum", getAppNum())
            .append("price", getPrice())
            .append("endTime", getEndTime())
            .append("payChannel", getPayChannel())
            .append("rate", getRate())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
