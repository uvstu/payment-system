package com.uvstu.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 应用管理对象 application
 * 
 * @author 郑伟滨
 * @date 2023-02-05
 */
public class SApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 应用ID */
    @Excel(name = "应用ID")
    private String appId;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String appName;

    /** 签名KEY */
    @Excel(name = "签名KEY")
    private String signKey;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private Integer payMethod;

    /** 同步通知地址 */
    @Excel(name = "同步通知地址")
    private String returnUrl;

    /** 异步通知地址 */
    @Excel(name = "异步通知地址")
    private String notifyUrl;

    /** 退款通知地址 */
    @Excel(name = "退款通知地址")
    private String refundUrl;

    /** 应用状态  1 停用  2 启用 */
    @Excel(name = "应用状态  1 停用  2 启用")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setAppName(String appName) 
    {
        this.appName = appName;
    }

    public String getAppName() 
    {
        return appName;
    }
    public void setSignKey(String signKey) 
    {
        this.signKey = signKey;
    }

    public String getSignKey() 
    {
        return signKey;
    }
    public void setPayMethod(Integer payMethod) 
    {
        this.payMethod = payMethod;
    }

    public Integer getPayMethod() 
    {
        return payMethod;
    }
    public void setReturnUrl(String returnUrl) 
    {
        this.returnUrl = returnUrl;
    }

    public String getReturnUrl() 
    {
        return returnUrl;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setRefundUrl(String refundUrl) 
    {
        this.refundUrl = refundUrl;
    }

    public String getRefundUrl() 
    {
        return refundUrl;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("appId", getAppId())
            .append("appName", getAppName())
            .append("signKey", getSignKey())
            .append("payMethod", getPayMethod())
            .append("returnUrl", getReturnUrl())
            .append("notifyUrl", getNotifyUrl())
            .append("refundUrl", getRefundUrl())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
