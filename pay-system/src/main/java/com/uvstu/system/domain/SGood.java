package com.uvstu.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 商品对象 s_good
 * 
 * @author zwb
 * @date 2023-02-01
 */
public class SGood extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 套餐名称 */
    @Excel(name = "套餐名称")
    private String goodName;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 描述 */
    @Excel(name = "描述")
    private String introduction;

    /** 支持渠道 */
    @Excel(name = "支持渠道")
    private String payChannel;

    /** 应用数量 */
    @Excel(name = "应用数量")
    private Integer appNum;

    /** 费率 */
    @Excel(name = "费率")
    private BigDecimal rate;

    /** 有效时间(月) */
    @Excel(name = "有效时间(月)")
    private Integer effectiveTime;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setIntroduction(String introduction)
    {
        this.introduction = introduction;
    }

    public String getIntroduction()
    {
        return introduction;
    }
    public void setPayChannel(String payChannel) 
    {
        this.payChannel = payChannel;
    }

    public String getPayChannel() 
    {
        return payChannel;
    }
    public void setAppNum(Integer appNum) 
    {
        this.appNum = appNum;
    }

    public Integer getAppNum() 
    {
        return appNum;
    }
    public void setRate(BigDecimal rate) 
    {
        this.rate = rate;
    }

    public BigDecimal getRate() 
    {
        return rate;
    }
    public void setEffectiveTime(Integer effectiveTime) 
    {
        this.effectiveTime = effectiveTime;
    }

    public Integer getEffectiveTime() 
    {
        return effectiveTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodName", getGoodName())
            .append("price", getPrice())
            .append("introduction", getIntroduction())
            .append("payChannel", getPayChannel())
            .append("appNum", getAppNum())
            .append("rate", getRate())
            .append("effectiveTime", getEffectiveTime())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
