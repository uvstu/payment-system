package com.uvstu.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 结算管理对象 s_settlement
 * 
 * @author zwb
 * @date 2023-02-12
 */
public class SSettlement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户 */
    @Excel(name = "用户")
    private Long userId;

    /** 昨天结算 */
    @Excel(name = "昨天结算")
    private String oneDays;

    /** 前天结算 */
    @Excel(name = "前天结算")
    private String towDays;

    /** 微信收款二维码 */
    @Excel(name = "微信收款二维码")
    private String wxImg;

    /** 支付宝收款二维码 */
    @Excel(name = "支付宝收款二维码")
    private String zfbImg;

    /** 结算状态 */
    @Excel(name = "结算状态")
    private String settlementStatus;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setOneDays(String oneDays) 
    {
        this.oneDays = oneDays;
    }

    public String getOneDays() 
    {
        return oneDays;
    }
    public void setTowDays(String towDays) 
    {
        this.towDays = towDays;
    }

    public String getTowDays() 
    {
        return towDays;
    }
    public void setWxImg(String wxImg) 
    {
        this.wxImg = wxImg;
    }

    public String getWxImg() 
    {
        return wxImg;
    }
    public void setZfbImg(String zfbImg) 
    {
        this.zfbImg = zfbImg;
    }

    public String getZfbImg() 
    {
        return zfbImg;
    }
    public void setSettlementStatus(String settlementStatus) 
    {
        this.settlementStatus = settlementStatus;
    }

    public String getSettlementStatus() 
    {
        return settlementStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("oneDays", getOneDays())
            .append("towDays", getTowDays())
            .append("wxImg", getWxImg())
            .append("zfbImg", getZfbImg())
            .append("settlementStatus", getSettlementStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
