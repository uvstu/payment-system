package com.uvstu.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.uvstu.common.annotation.Excel;
import com.uvstu.common.core.domain.BaseEntity;

/**
 * 用户订单对象 s_uorder
 * 
 * @author zwb
 * @date 2023-02-07
 */
public class SUorder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户ID */
    private Long userId;

    /** 应用ID */
    private String appId;

    /** 商户订单号 */
    @Excel(name = "商户订单号")
    private String outTradeNo;

    /** 第三方订单号 */
    @Excel(name = "第三方订单号")
    private String tradeNo;

    /** 第三方退款单号 */
    @Excel(name = "第三方退款单号")
    private String refundNo;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 支付渠道 */
    @Excel(name = "支付渠道")
    private Integer payMethod;

    /** 付款金额 */
    @Excel(name = "付款金额")
    private BigDecimal price;

    /** 支付响应参数 */
    @Excel(name = "支付响应参数")
    private String payParam;

    /** 退款响应参数 */
    @Excel(name = "退款响应参数")
    private String refundParam;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 交易时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交易时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date transactionTime;

    /** 退款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "退款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date refundTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getAppId()
    {
        return appId;
    }
    public void setOutTradeNo(String outTradeNo) 
    {
        this.outTradeNo = outTradeNo;
    }

    public String getOutTradeNo() 
    {
        return outTradeNo;
    }
    public void setTradeNo(String tradeNo) 
    {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() 
    {
        return tradeNo;
    }
    public void setRefundNo(String refundNo)
    {
        this.refundNo = refundNo;
    }

    public String getRefundNo()
    {
        return refundNo;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setPayMethod(Integer payMethod) 
    {
        this.payMethod = payMethod;
    }

    public Integer getPayMethod() 
    {
        return payMethod;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setPayParam(String payParam) 
    {
        this.payParam = payParam;
    }

    public String getPayParam() 
    {
        return payParam;
    }
    public void setRefundParam(String refundParam) 
    {
        this.refundParam = refundParam;
    }

    public String getRefundParam() 
    {
        return refundParam;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setTransactionTime(Date transactionTime) 
    {
        this.transactionTime = transactionTime;
    }

    public Date getTransactionTime() 
    {
        return transactionTime;
    }
    public void setRefundTime(Date refundTime) 
    {
        this.refundTime = refundTime;
    }

    public Date getRefundTime() 
    {
        return refundTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("appId", getAppId())
            .append("outTradeNo", getOutTradeNo())
            .append("tradeNo", getTradeNo())
            .append("refundNo", getRefundNo())
            .append("goodName", getGoodName())
            .append("payMethod", getPayMethod())
            .append("price", getPrice())
            .append("payParam", getPayParam())
            .append("refundParam", getRefundParam())
            .append("status", getStatus())
            .append("transactionTime", getTransactionTime())
            .append("refundTime", getRefundTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .toString();
    }
}
